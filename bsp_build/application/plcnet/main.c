#include "pub_define.h"
#include "debug_print.h"
#include "uart.h"
#include "platform.h"
#include "cloud_node_opt.h"
#include "node_group.h"
#include "plc_poll_common.h"
#include "plc_ppi_rw.h"
#include "plc_fx_rw.h"
#include "plc_fx_test.h"

#if MACHINE_TYPE == MACHINE_ZX7520
void *feed_dog(void *arg)
{
    while(1)
    {
        usr_dog_feed();
        sleep(1);
    }
}
#endif

int main(void)
{
    uart_init();
    init_dbg();
    set_dbg_limit(APP_DEBUG);

#if MACHINE_TYPE == MACHINE_ZX7520
    pthread_t tid;
    pthread_create(&tid, NULL, feed_dog, NULL);
#endif

#if 0  //测FX协议读写封装
//    test_fx_read();
    unsigned char buf[256];
    fx_plc_read('D', 8249, 1, REG_WORD, buf);
#endif

#if 0 //fins协议测试
    test_fins_read();
    int i;
    unsigned char buf[100] = {0};
    int ret = fins_plc_read(1, CIO_REG_TYPE_R, 0, 1, REG_BIT, buf);
    DBG_LEVEL_STD(APP_DEBUG, FX_RW_DBSW, FX_RW_NAME, "fx read from actual uart len = %d", ret);
    for(i=0; i<ret; i++)
    {
        DBG_PRINT(APP_DEBUG FX_RW_DBSW "%02X ", buf[i]);
    }
    DBG_PRINT(APP_DEBUG FX_RW_DBSW "\n");


    fins_plc_write(1, DM_REG_TYPE, 100, 1, REG_DWORD, 0x12343354);
#endif

#if 1 //测试PPI协议
    unsigned char buf[100] = {0};
    ppi_plc_read(2, 'Q', 0, 1, REG_BIT, buf);
//    int val = 0xabcd;
//    int val = 123456;
//    while(1){
//        ppi_plc_write(2, 'V', 0, 1, REG_DWORD, &val);
//        val++;
//        sleep(1);
//    }
#endif

#if 0 //测节点合并
    init_poll();
    init_clound_node();
    node_merge_init();
    plc_poll_run(NULL);
#endif


    while (1) {
        sleep(1);
    }

    return 0;
}












