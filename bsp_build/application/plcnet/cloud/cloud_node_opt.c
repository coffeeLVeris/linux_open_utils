#include "cloud_node_opt.h"
#include "file_opt.h"
#include "cJSON.h"
#include "plc_mb_group.h"

static struct cloud_node_obj node_arr[MAX_NODE_NUM];
static int available_num = 0; //代表数组项中真实可用的节点数目

/**
 * @brief 数组初始化
 */
void CloudNodeOpt_Init(void)
{
    memset(node_arr, 0, NODE_ARR_SIZE);
}

/**
 * @brief 结点清除操作
 * @param num 要清除的结点个数
 * @return 0 成功，-1失败
 */
int CloudNodeOpt_Clear(int num)
{
    if(num > MAX_NODE_NUM)
    {
        DBG_LEVEL_APP(APP_ERR, CN_NAME, "not support this num node");
        return -1;
    }

    memset(node_arr, 0, CALC_ARR_SIZE(num));
    return 0;
}

/**
 * @brief 获取支持的最大数目
 * @return
 */
int CloudNodeOpt_GetMaxNum(void)
{
    return MAX_NODE_NUM;
}

/**
 * @brief 获取某个结点信息
 * @param index
 * @return 这里开销也不大，直接返回结构体，防止修改原始数组
 */
struct cloud_node_obj CloudNodeOpt_GetNode(int index)
{
    assert(index < MAX_NODE_NUM);
    return node_arr[index];
}

/**
 * @brief 修改某个结点的信息:因为共享区域node_arr并没有做任何互斥操作，所以尽量不要用这个接口，用下面细分接口
 * @param index
 * @return 这里开销也不大，直接返回结构体，防止修改原始数组
 */
int CloudNodeOpt_UpdateNode(int index, struct cloud_node_obj new_node)
{
    if(index >= MAX_NODE_NUM){
        DBG_LEVEL_APP(APP_ERR, CN_NAME, "error index");
        return -1;
    }

    node_arr[index] = new_node;
    return 0;
}

int CloudNodeOpt_UpOldVal(int index, int old_val)
{
    if(index >= MAX_NODE_NUM){
        DBG_LEVEL_APP(APP_ERR, CN_NAME, "error index");
        return -1;
    }

    node_arr[index].old_val = old_val;
    return 0;
}

int CloudNodeOpt_UpCurVal(int index, int cur_val)
{
    if(index >= MAX_NODE_NUM){
        DBG_LEVEL_APP(APP_ERR, CN_NAME, "error index");
        return -1;
    }

    node_arr[index].cur_val = cur_val;
    return 0;
}

int CloudNodeOpt_UpAllVal(int index, int val)
{
    if(index >= MAX_NODE_NUM)
    {
        DBG_LEVEL_APP(APP_ERR, CN_NAME, "error index");
        return -1;
    }

    node_arr[index].cur_val = val;
    node_arr[index].old_val = val;
    return 0;
}

void CloudNodeOpt_SetAvlNum(int num)
{
    available_num = num;
}

/**
 * @brief 返回的是有效节点数，但是节点数是从0开始的，注意
 * @return
 */
int CloudNodeOpt_GetAvlNum(void)
{
    return available_num;
}

/**
 * @brief 显示所有的结点信息，调试使用
 * @param start
 * @param end
 */
void CloudNodeOpt_Show(int start, int end)
{
    int i;
    DBG_PRINT(APP_INFO CN_DBSW "\n");
    DBG_LEVEL_STD(APP_INFO, CN_DBSW, CN_NAME, "all node info");
    for(i=start; i<end; i++)
    {
        printf("---------------------------------------------------------------------------------------------\n");
        printf("num = %d ", i);
        printf("sid = %d, mdbs_addr=%d,protocol=%d,plc_regtype=%c,plc_regnumber=%d,datatype=%d\n",
               node_arr[i].sid, node_arr[i].mdbs_addr, node_arr[i].protocol, \
               node_arr[i].plc_regtype, node_arr[i].plc_regnumber, node_arr[i].datatype);
    }

    printf("---------------------------------------------------------------------------------------------\n");
    printf("\n");
}

/**
 * @brief 查找并且更新节点信息
 * @param node
 * @return 0：找到并且更新成功，-1：未找到这个点
 */
int CloudNodeOpt_FdAndUpNode(struct cloud_node_obj node)
{
    int i;
    for(i=0; i<available_num; i++){
        if(node_arr[i].plc_regtype == node.plc_regtype && node_arr[i].plc_regnumber == node.plc_regnumber){
            //如果匹配成功了，那么更新节点数据
            node_arr[i].cur_val = node.cur_val;
            node_arr[i].old_val = node.cur_val;
            return 0;
        }
    }

    //到此代表找不到这个点，属于错误
    return -1;
}

/*-----------------------以下都是云端节点操作----------------------*/
/**
 * @brief 解析某个int型节点数据
 * @param json_str json字符串
 * @param node_name 节点名称
 * @return 成功：节点值 失败：-1
 */
static int CloudNodeOpt_ParseIntNode(const char *json_str, const char *node_name)
{
    cJSON * root = NULL;

    root = cJSON_Parse(json_str);
    if(NULL == root)
    {
        DBG_LEVEL_APP(APP_ERR, CN_NAME, "parse error, check json_str");
        return -1;
    }

    int num = cJSON_GetObjectItem(root, node_name)->valueint;
    return num;
}


static inline int CloudNodeOpt_ParseUptm(const char *json_str)
{
    return CloudNodeOpt_ParseIntNode(json_str, "uptm");
}

/**
 * @brief 解析传过来的json字符串并顺序存储到数组中
 * @param json_str json字符串
 * @return
 */
static int CloudNodeOpt_ParseJsonAndSave(const char *json_str)
{
    int i;
    cJSON * root = NULL;
    cJSON * item = NULL;

    root = cJSON_Parse(json_str);
    if(NULL == root)
    {
        //DBG_LEVEL_APP(APP_ERR, CN_NAME, "json_str = %s", json_str);
        DBG_LEVEL_APP(APP_ERR, CN_NAME, "parse error, check json_str");
        return -1;
    }

    //printf("recv json = %s\n\n", cJSON_Print(root));

    int status = cJSON_GetObjectItem(root, "status")->valueint;
    if(status != 0)
    {
        if(status == 2014)
        {
            DBG_LEVEL_APP(APP_ERR, CN_NAME, "no device");
        }
        else if(status == 1000)
        {
            DBG_LEVEL_APP(APP_ERR, CN_NAME, "secret error");
        }
        else{

        }

        return -1;
    }

    int curr_num = cJSON_GetObjectItem(root, "curr_num")->valueint;

    item = cJSON_GetObjectItem(root, "data");  //获取到data数组域
    if(CloudNodeOpt_Clear(curr_num) < 0)  //清除一下数组项
    {
        return -1;
    }

    cJSON *data_list = item->child;  //指向第一个数组项

    for(i=0; i<curr_num; i++)
    {
        if(NULL != data_list)
        {
            node_arr[i].sid = (char)cJSON_GetObjectItem(data_list, "sid")->valueint;
            node_arr[i].mdbs_addr = atoi(cJSON_GetObjectItem(data_list, "reg")->valuestring); //不能直接用上面的int，因为这个成语类型是string
            node_arr[i].datatype = (char)cJSON_GetObjectItem(data_list, "vk")->valueint;
            node_arr[i].plc_regnumber = mb_addr_translate(node_arr[i].mdbs_addr, &node_arr[i].plc_regtype);

            data_list = data_list->next;
        }
        else
        {
            DBG_LEVEL_APP(APP_ERR, CN_NAME, "parse error");
            return -1;
        }
    }

    available_num = curr_num;

    return 0;
}

int init_clound_node()
{
    //从文件中读取所有节点数据
    int filesize = FileOpt_GetSize(SAVE_FILE_PATH);
    char *read_buf = (char *)malloc(filesize);
    if(read_buf == NULL){
        return -1;
    }
    memset(read_buf, 0, filesize);
    FileOpt_ReadAll(SAVE_FILE_PATH, read_buf);
//    printf("read buf =    %s\n", read_buf);

    //json解析并存储
    if(CloudNodeOpt_ParseJsonAndSave(read_buf) < 0){
        DBG_LEVEL_APP(APP_ERR, CN_NAME, "parse error");
        return -1;
    }

    //调试用，可以打印全局数组中的内容
    CloudNodeOpt_Show(0, CloudNodeOpt_GetAvlNum());

    return 0;
}
