/***********************************************************************************
Copy right:	2016-2026 USR IOT Tech.
Author:			焦岳
Version:		V1.0
Date:			2018-12
Description:	云端请求结点存储的公共链表，供各个线程数据提取和写入
***********************************************************************************/

#ifndef __CLOUD_NODE_OPT_H
#define __CLOUD_NODE_OPT_H

#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "debug_print.h"

/*
    字符	    plc地址			Modbus地址		寄存器类型	数据类型	    读写类型
            S0-999			1-1000			开关型					读写
            Y0-177			1001-1178		开关型					读写
            T0-255			1201-1456		开关型					读写
            C0-255			1501-1756		开关型					读写
            M0-M1023		2001-3024		开关型					读写
            M8000-M8255		8001-8256		开关型					读写
            X0-177			10001-10178		开关型					只读
            D0-D7999		40001-48000		数值型		2字节		读写
            D8000-D8255		48001-48256		数值型		2字节		只读
    't'		T0-255(值)		48501-48756		数值型		2字节		只读
    'c'		C0-199(值)		49001-49256		数值型		2字节		只读
*/

enum PTC_TYPE{
    FX = 0,
    PPI,
    MODBUS
};

#define SAVE_FILE_PATH "data.tmp"

/**
 * @brief 云端数据结点定义
 * name:模块名称
 */
struct cloud_node_obj{
    char sid;
    int  mdbs_addr; //modbus寄存器地址
    char mdbs_cmd;  //暂时空
    char protocol;  //协议类型FX ppi modbus
    char plc_regtype;   //根据规则计算的字符：比如40001对应就是'D'
    int  plc_regnumber; //根据规则计算的地址：比如40001对应就是 0  这个值应该没用了，确认后可删除@5.31 jiaoyue
    char datatype;  //暂时空
    char readwrite; //暂时空
    char rlen;  //暂时空
    int  old_val;  //上一次的值
    int  cur_val;  //最新查到的值
    char sta;//fangyufei@20190114用于记录当前结点状态
};

//added by wangbin begin
typedef struct cloud_node_obj ProtocolTemplate_t;

typedef struct _protocol_data{
    const void *val_in; //输入参数
    void *val_out;      //输出参数
    int   reg_sum;      //寄存器数量，分为位寄存器、双字节、四字节
} ProtocolData_t;
//added by wangbin end

#define MAX_NODE_NUM 500  //支持的最大结点数量个数
#define NODE_ARR_SIZE (MAX_NODE_NUM * sizeof(struct cloud_node_obj))
#define CALC_ARR_SIZE(num) ((num) * sizeof(struct cloud_node_obj))

//调试控制
#define CN_DBSW MODULE_DBG_ON
#define CN_NAME "cloud_node_opt"

void CloudNodeOpt_Init(void);
int CloudNodeOpt_Clear(int num);
int CloudNodeOpt_GetMaxNum(void);
struct cloud_node_obj CloudNodeOpt_GetNode(int index);
int CloudNodeOpt_UpdateNode(int index, struct cloud_node_obj new_node);
int CloudNodeOpt_UpOldVal(int index, int old_val);
int CloudNodeOpt_UpCurVal(int index, int cur_val);
int CloudNodeOpt_UpAllVal(int index, int val);
void CloudNodeOpt_Show(int start, int end);
void CloudNodeOpt_SetAvlNum(int num);
int CloudNodeOpt_GetAvlNum(void);
int CloudNodeOpt_FdAndUpNode(struct cloud_node_obj node);

/*-----------------------以下都是云端节点操作----------------------*/
int init_clound_node();

#endif
