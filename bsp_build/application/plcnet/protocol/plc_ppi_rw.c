/***********************************************************************************
Copy right:     2016-2026 USR IOT Tech.
Author:         焦岳
Version:        V1.0
Date:           2019-07
Description:    ppi协议读写
***********************************************************************************/

#include "uart.h"
#include "plc_ppi_rw.h"
#include "algo_common.h"
#include "byte_array.h"
#include "plc_poll_common.h"
#include "Crc_Calc.h"
#include <arpa/inet.h>

/**
 * @brief 写寄存器
 * @param sid
 * @param reg_type 寄存器类型
 * @param start 寄存器起始地址，如Q1.5=1*8+5=13
 * @param number 寄存器个数
 * @param data_type 数据类型 参考：REG_T
 * @param buf 数据流
 * @return
 */
int ppi_plc_read(char sid, char reg_type, int start, int number, char data_type, unsigned char *buf)
{
    int i;
    int start_tmp = start;
    uint8_t rsp_buf[256];
    unsigned char send[]={0x68,0x1B,0x1B,0x68,0x02,0x00,0x6C,0x32,0x01,0x00,\
                          0x00,0x00,0x00,0x00,0x0E,0x00,0x00,0x04,0x01,0x12,\
                          0x0A,0x10,0x02,0x00,0x08,0x00,0x00,0x03,0x00,0x05,\
                          0xE0,0xD2,0x16};
    unsigned char ack[]={0x10,0x00,0x00,0x5C,0x5E,0x16};
    ack[1] = sid;
    int act_num = number; //实际读取个数，对于位的会发生变化
    int must_recv_len = 0;  //应该读到的字节数

    //从机地址
    send[4] = sid;

    //数据类型
    switch (data_type) {
    case REG_BIT:
        if(number > 1){
            send[22] = 2;  //位寄存器不能读多个位，只能按照字节读取后进行处理
            int start_addr = (start / 8) * 8;
            int end_addr = ((start_addr + number - 1) / 8) * 8;
            act_num = ((end_addr - start_addr) / 8) + 1;
            must_recv_len = act_num; //字节数
            start = start_addr;
        }else{
            send[22] = 1;
            must_recv_len = act_num;
        }
        break;
    case REG_BYTE:
        send[22] = 2;
        must_recv_len = act_num;
        break;
    case REG_WORD:
        send[22] = 4;
        must_recv_len = act_num * 2;
        break;
    case REG_DWORD:
        send[22] = 6;
        must_recv_len = act_num * 4;
        break;
    default:
        break;
    }

    //读取数据个数
    send[23] = (act_num >> 8) & 0xff;
    send[24] = act_num & 0xff;

    //DBnumber
    send[25] = 0;
    if(reg_type == PPI_V_REG){
        send[26] = 1;
    }else{
        send[26] = 0;
    }

    switch (reg_type) {
    case PPI_I_REG:
        send[27] = 0x81;
        break;
    case PPI_Q_REG:
        send[27] = 0x82;
        break;
    case PPI_M_REG:
        send[27] = 0x83;
        break;
    case PPI_V_REG:
        send[27] = 0x84;
        start *= 8;  //对于按照字节/字/双字读时候，偏移需要*8
        break;
    default:
        //其他的暂不支持
        DBG_LEVEL_STD(APP_ERR, PLC_PRO_DBG, PLC_PRO_NAME, "type error");
        return -1;
        break;
    }

    //偏移指针
    send[28] = start / 65536;
    send[29] = start / 256;
    send[30] = start % 256;

    //校验和
    int check_sum = calc_chesum(send, 4, 30);
    send[31] = check_sum;

    int cmd_len = 33;
    DBG_LEVEL_STD(APP_DEBUG, PLC_PRO_DBG, PLC_PRO_NAME, "mb send to PLC len = %d", cmd_len);
    for(i=0; i<cmd_len; i++)
    {
        DBG_PRINT(APP_DEBUG PLC_PRO_DBG "%02X ", send[i]);
    }
    DBG_PRINT(APP_DEBUG PLC_PRO_DBG "\n");

    int ret = uart_write(send, cmd_len);
    if(ret < 0){
        DBG_LEVEL_STD(APP_ERR, PLC_PRO_DBG, PLC_PRO_NAME, "write err");
        return -1;
    }

    usleep(2000);
    int recv_len = uart_read(rsp_buf, 256);

    DBG_LEVEL_STD(APP_DEBUG, PLC_PRO_DBG, PLC_PRO_NAME, "recv from plc len = %d", recv_len);
    for(i=0; i<recv_len; i++){
        DBG_PRINT(APP_DEBUG PLC_PRO_DBG "%02X ", rsp_buf[i]);
    }
    DBG_PRINT(APP_DEBUG PLC_PRO_DBG "\n");

    //判断E5回应
    if(rsp_buf[0] != 0xE5){
        DBG_LEVEL_STD(APP_ERR, PLC_PRO_DBG, PLC_PRO_NAME, "RECV_4_PLC error");
        return -1;
    }

    DBG_LEVEL_STD(APP_DEBUG, PLC_PRO_DBG, PLC_PRO_NAME, "recv send plc ack = %d", 6);
    for(i=0; i<6; i++){
        DBG_PRINT(APP_DEBUG PLC_PRO_DBG "%02X ", ack[i]);
    }
    DBG_PRINT(APP_DEBUG PLC_PRO_DBG "\n");

    //回复成功后再发送ack包
    ret = uart_write(ack, 6);
    if(ret < 0){
        DBG_LEVEL_STD(APP_ERR, PLC_PRO_DBG, PLC_PRO_NAME, "write err");
        return -1;
    }

    //再接收就是实际读到的数据了
    usleep(100000);
    recv_len = uart_read(rsp_buf, 256);

    DBG_LEVEL_STD(APP_DEBUG, PLC_PRO_DBG, PLC_PRO_NAME, "recv from plc len = %d", recv_len);
    for(i=0; i<recv_len; i++){
        DBG_PRINT(APP_DEBUG PLC_PRO_DBG "%02X ", rsp_buf[i]);
    }
    DBG_PRINT(APP_DEBUG PLC_PRO_DBG "\n");

    //简单校验一下，这里协议没有明确给出校验方法
    if(rsp_buf[0]!=0x68 || rsp_buf[3]!=0x68){
        DBG_LEVEL_STD(APP_ERR, PLC_PRO_DBG, PLC_PRO_NAME, "recv err");
        return -1;
    }

    //数据正确，取出需要的数据返回即可
    unsigned char *data = rsp_buf + 25;
    if(data_type == REG_BIT){  //如果是位寄存器，需要做移位操作
        if(number > 1){
            get_bit(buf, data, start_tmp%8, number);
        }else{
            get_bit(buf, data, 0, 1); //只读一个位的话，一定是从0开始拷贝
        }
    }else{
        memcpy(buf, data, must_recv_len);  //如果不是位寄存器，按实际字节数拷贝即可
    }

    DBG_LEVEL_STD(APP_DEBUG, PLC_PRO_DBG, PLC_PRO_NAME, "buf[0]=%02x", buf[0]);

    //最后的存在buf中
    DBG_LEVEL_STD(APP_DEBUG, PLC_PRO_DBG, PLC_PRO_NAME, "final len = %d", must_recv_len);
    for(i=0; i<must_recv_len; i++){
        DBG_PRINT(APP_DEBUG PLC_PRO_DBG "%02X ", buf[i]);
    }
    DBG_PRINT(APP_DEBUG PLC_PRO_DBG "\n");

    return must_recv_len;
}

/**
 * @brief 写寄存器
 * @param sid
 * @param reg_type
 * @param start 寄存器起始地址，如Q1.5=1*8+5=13
 * @param number 现只支持写一个
 * @param data_type 数据类型 参考：REG_T
 * @param data 原始数据，这里按照小端处理
 * @return
 */
int ppi_plc_write(char sid, char reg_type, int start, int number, char data_type, const void *data)
{
    int i, ret, recv_len;
    int cur_pos = 0;  //数据长度，字节
    int num_bit; //写入数据位数
    uint8_t rsp_buf[256];
    unsigned int conver_val = 0;  //大小端转换后数据，这个根据实际情况来，如果不需要转换这个变量没用，直接用data

    unsigned char send[64]={0x68,0x00,0x00,0x68,0x00,0x00,0x7C,0x32,0x01,0x00,\
                            0x00,0x00,0x00,0x00,0x0E,0x00,0x00,0x05,0x01,0x12,\
                            0x0A,0x10};

    unsigned char ack[]={0x10,0x02,0x00,0x5C,0x5E,0x16};

    if(number != 1){
        DBG_LEVEL_STD(APP_ERR, PLC_PRO_DBG, PLC_PRO_NAME, "data_type error");
        return -1;
    }

    //目标地址
    send[4] = sid;

    //数据类型
    switch (data_type) {
    case REG_BIT:
        send[16] = 5;  //写入的是位或者字节
        send[22] = 1;  //表示位
        num_bit = 1;
        conver_val = *((unsigned char *)data);
        break;
    case REG_BYTE:
        send[16] = 5;  //写入的是位或者字节
        send[22] = 2;  //表示字节
        num_bit = 8;
        conver_val = *((unsigned char *)data);
        break;
    case REG_WORD:
        send[16] = 6;  //写入的是字
        send[22] = 4;  //表示字
        num_bit = 16;
        conver_val = htons(*((unsigned short *)data));
        break;
    case REG_DWORD:
        send[16] = 8;  //写入的是双字
        send[22] = 6;  //表示双字
        num_bit = 32;
        conver_val = htonl(*((unsigned int *)data));
        break;
    default:
        //其他的暂不支持
        DBG_LEVEL_STD(APP_ERR, PLC_PRO_DBG, PLC_PRO_NAME, "data_type error");
        return -1;
        break;
    }

    //数据个数
    send[23] = 0;
    send[24] = 1;

    //DB号码
    if(reg_type == PPI_V_REG){
        send[25] = 0;
        send[26] = 1;
    }else{
        send[25] = 0;
        send[26] = 0;
    }

    //根据寄存器类型决定存储类型
    switch (reg_type) {
    case PPI_I_REG:
        send[27] = 0x81;
        break;
    case PPI_Q_REG:
        send[27] = 0x82;
        break;
    case PPI_M_REG:
        send[27] = 0x83;
        break;
    case PPI_V_REG:
        send[27] = 0x84;
        start *= 8;  //对于按照字节/字/双字读时候，偏移需要*8
        break;
    default:
        //其他的暂不支持
        DBG_LEVEL_STD(APP_ERR, PLC_PRO_DBG, PLC_PRO_NAME, "type error");
        return -1;
        break;
    }

    //偏移指针
    send[28] = start / 65536;
    send[29] = start / 256;
    send[30] = start % 256;

    //数据形式：03表示位，04表示其它
    send[31] = 0;
    if(data_type == REG_BIT){
        send[32] = 3;
    }else{
        send[32] = 4;
    }

    //数据位数
    send[33] = 0;
    send[34] = num_bit;

    cur_pos = 34;  //当前游标

    switch (data_type) {
    case REG_BIT:
        memcpy(send+35, &conver_val, 1); //位一定是一个字节
        cur_pos += 1;
        break;
    case REG_BYTE:
        memcpy(send+35, &conver_val, 1);
        cur_pos += 1;
        break;
    case REG_WORD:
        memcpy(send+35, &conver_val, 2);
        cur_pos += 2;
        break;
    case REG_DWORD:
        memcpy(send+35, &conver_val, 4);
        cur_pos += 4;
        break;
    default:
        //其他的暂不支持
        DBG_LEVEL_STD(APP_ERR, PLC_PRO_DBG, PLC_PRO_NAME, "data_type error");
        return -1;
        break;
    }

    send[cur_pos + 1] = calc_chesum(send, 4, cur_pos);  //校验码
    send[cur_pos + 2] = 0x16; //终止符

    //最后确定整个报文的长度
    int total_len = cur_pos + 3;
    send[1] = total_len - 6;
    send[2] = total_len - 6;

    //到此所有组包完毕，开始发送
    //先发送指令
    DBG_LEVEL_STD(APP_DEBUG, PLC_PRO_DBG, PLC_PRO_NAME, "send cmd len = %d", total_len);
    for(i=0; i<total_len; i++){
        DBG_PRINT(APP_DEBUG PLC_PRO_DBG "%02X ", send[i]);
    }
    DBG_PRINT(APP_DEBUG PLC_PRO_DBG "\n");

    ret = uart_write(send, total_len);
    if(ret < 0){
        DBG_LEVEL_STD(APP_ERR, PLC_PRO_DBG, PLC_PRO_NAME, "write err");
        return -1;
    }

    //等待回复E5
    usleep(100000);
    recv_len = uart_read(rsp_buf, 10);

    DBG_LEVEL_STD(APP_DEBUG, PLC_PRO_DBG, PLC_PRO_NAME, "recv from plc len = %d", recv_len);
    for(i=0; i<recv_len; i++){
        DBG_PRINT(APP_DEBUG PLC_PRO_DBG "%02X ", rsp_buf[i]);
    }
    DBG_PRINT(APP_DEBUG PLC_PRO_DBG "\n");

    //判断E5回应
    if(rsp_buf[0] != 0xE5){
        DBG_LEVEL_STD(APP_ERR, PLC_PRO_DBG, PLC_PRO_NAME, "RECV_4_PLC error");
        return -1;
    }else{
        DBG_LEVEL_STD(APP_DEBUG, PLC_PRO_DBG, PLC_PRO_NAME, "write success");
    }

    //再发送ack表示本次写完成
    usleep(2000);
    DBG_LEVEL_STD(APP_DEBUG, PLC_PRO_DBG, PLC_PRO_NAME, "send ack len = %d", 6);
    for(i=0; i<6; i++){
        DBG_PRINT(APP_DEBUG PLC_PRO_DBG "%02X ", ack[i]);
    }
    DBG_PRINT(APP_DEBUG PLC_PRO_DBG "\n");

    uart_write(ack, 6);

    //成功会返回68 12 12 68 00 02 08 32 03 00 0000 00 00 02 00 01 00 00 05 01 FF 47 16，清掉它
    usleep(100000);
    recv_len = uart_read(rsp_buf, 256);

    DBG_LEVEL_STD(APP_DEBUG, PLC_PRO_DBG, PLC_PRO_NAME, "recv from plc len = %d", recv_len);
    for(i=0; i<recv_len; i++){
        DBG_PRINT(APP_DEBUG PLC_PRO_DBG "%02X ", rsp_buf[i]);
    }
    DBG_PRINT(APP_DEBUG PLC_PRO_DBG "\n");

    return 0;
}
