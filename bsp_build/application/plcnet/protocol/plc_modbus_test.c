/**
 ** @author:   焦岳
 ** @date:	   2019.1.3
 ** @brief:    MODBUS测试程序
 */
 
#include "plc_modbus_test.h"
#include "uart.h"
#include "debug_print.h"
#include "Crc_Calc.h"

#define MODBUS_MAX_READ 256

int test_mb_read(int sid, int reg_start_addr, int reg_len, int type)
{
    uint8_t cmd_buf[8];
    int cmd_len;
    uint16_t crc_ret;
    int addr;
    char buf[MODBUS_MAX_READ];
    int nbytes, i;

    //检查从机地址范围，必须为0~255
    if ((sid < 0) || (sid > 255))
    {
        DBG_LEVEL_STD(APP_INFO, COMMON_DBSW, COMMON_DBNM, "sid error");
        return -1;
    }

    //检查寄存器地址范围，必须为0~65535
    if ((reg_start_addr < 0) || (reg_start_addr > 65535))
    {
        DBG_LEVEL_STD(APP_INFO, COMMON_DBSW, COMMON_DBNM, "reg_start_addr error");
        return -1;
    }

    //检查寄存器长度，必须为0~65535
    if ((reg_len < 0) || (reg_len > 65535))
    {
        DBG_LEVEL_STD(APP_INFO, COMMON_DBSW, COMMON_DBNM, "reg_len error");
        return -1;
    }


    //设置从机地址
    cmd_buf[0] = (uint8_t)sid;

    //设置功能码
    if (reg_start_addr >= MB_START_0 && reg_start_addr <= MB_END_0) //线圈寄存器
    {
        cmd_buf[1] = 0x01;
    }
    else if (reg_start_addr >= MB_START_1 && reg_start_addr <= MB_END_1) //离散寄存器
    {
        cmd_buf[1] = 0x02;
    }
    else if (reg_start_addr >= MB_START_4 && reg_start_addr <= MB_END_4) //保持寄存器
    {
        cmd_buf[1] = 0x03;
    }
    else if (reg_start_addr >= MB_START_3 && reg_start_addr <= MB_END_3) //输入寄存器
    {
        cmd_buf[1] = 0x04;
    }
    else //地址错误
    {
        return -1;
    }

    switch(type)
    {
        case 2:
        case 3:
        case 4:
        case 6:
        case 8:
        case 10:
        {
            reg_len *= 2;
            break;
        }

        default:
        {
            break;
        }
    }

    //计算地址和长度
    cmd_len = 8;
    addr = reg_start_addr % 10000 - 1;
    cmd_buf[2] = (uint8_t)((uint16_t)addr >> 8);
    cmd_buf[3] = (uint8_t)((uint16_t)addr & 0x00FF);
    cmd_buf[4] = (uint8_t)((uint16_t)reg_len >> 8);
    cmd_buf[5] = (uint8_t)((uint16_t)reg_len & 0x00FF);

    //计算CRC
    crc_ret = GetCRC16(cmd_buf, cmd_len-2);
    cmd_buf[6] = (uint8_t)(crc_ret >> 8);
    cmd_buf[7] = (uint8_t)(crc_ret & 0x00FF);

    int ret = uart_write(cmd_buf, cmd_len);
    if(ret < 0){
        perror("write err");
        return -1;
    }

    nbytes = uart_read(buf, MODBUS_MAX_READ);

    DBG_LEVEL_STD(APP_INFO, COMMON_DBSW, COMMON_DBNM, "recv nbytes = %d", nbytes);
    for(i=0; i<nbytes; i++)
    {
        DBG_PRINT(APP_INFO COMMON_DBSW "%02X ", buf[i]);
    }
    DBG_PRINT(APP_INFO COMMON_DBSW "\n");

    return 0;
}
