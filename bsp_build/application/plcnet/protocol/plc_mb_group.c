/***********************************************************************************
Copy right:     2016-2026 USR IOT Tech.
Author:         焦岳
Version:        V1.0
Date:           2019-06
Description:    modbus分组子层文件
***********************************************************************************/

#include "plc_mb_rw.h"
#include "plc_mb_group.h"
#include "debug_print.h"
#include "plc_poll_common.h"

#define MB_MAX_RD_CELL 32

static int mb2addr_space_map(struct group_info *group_info)
{
    group_info->reg_addr_space = 1;
    switch (group_info->vk)
    {
        case 0:
        case 1:
        case 5:
            group_info->reg_addr_space = 1;
            break;
        case 2:
        case 3:
        case 4:
        case 6:
        case 8:
        case 10:
            group_info->reg_addr_space = 2;
            break;
        default:
        DBG_LEVEL_APP(APP_ERR, NM_NAME,"not support vk = %d", group_info->vk);
        return -1;
    }
    return 0;
}

static int mb_read_session(struct group_info *gp_info)
{
    int len;
    int start_mbaddr = gp_info->group_start_addr;
    int reg_num = gp_info->group_limit_nc;
    int reg_type = gp_info->vk;
    len = modbus_plc_read(gp_info->sid, start_mbaddr, reg_num, reg_type, gp_info->new_data_buf);
    if(len < 0){
        DBG_LEVEL_STD(APP_ERR, NM_DBSW, NM_NAME, "groupIdx = %d, start_mbaddr = %d read error",\
                      gp_info->idx, start_mbaddr);
        return -1;
    }

    gp_info->new_len = len;

    DBG_LEVEL_STD(APP_DEBUG, NM_DBSW, NM_NAME, \
                  "groupIdx = %d, start_mbaddr = %d, reg_num = %d, reg_type = %d, len_bytes = %d",\
                  gp_info->idx, start_mbaddr, reg_num, reg_type, len);

    int i;
    DBG_LEVEL_STD(APP_DEBUG, NM_DBSW, NM_NAME, "mb read from PLC final result bytes = %d", gp_info->new_len);
    for(i=0; i<gp_info->new_len; i++)
    {
        DBG_PRINT(APP_DEBUG NM_DBSW "%02X ", gp_info->new_data_buf[i]);
    }
    DBG_PRINT(APP_DEBUG NM_DBSW "\n");

    return 0;
}

static int mb_register2poll(struct group_info *group_info)
{
    //调用plc的会话函数
    struct plc_poll_obj *poll_obj = pool_get_node();
    poll_obj->type = POLL_T;
    poll_obj->group = group_info;
    poll_obj->read_session = mb_read_session;

    //经过命令转换后存入plc_data_obj的plc_cmd_buf中,把这个节点加入到轮询链表中
    add_poll_obj(poll_obj);

    return 0;
}

/**
 * @brief mdbus主动上报接口
 * @param tmpRegHeadP
 * @param group_info
 * @return
 */
static int mb_active_report_fun(struct group_info *group_info)
{
    return 0;
}

/**
 * @brief 把所有的寄存器类型都注册进去
 * @return
 */
int node_merge_init_modbus()
{
    struct reg_type reg_tmp;
    memset(&reg_tmp, 0, sizeof(struct reg_type));

    //初始化公共部分
    reg_tmp.reg_max_count = MB_MAX_RD_CELL;
    reg_tmp.reg_report_fun = mb_active_report_fun;
    reg_tmp.set_addr_space = mb2addr_space_map;
    reg_tmp.register2poll = mb_register2poll;

    reg_tmp.type = MB_ADDR_0;
    reg_tmp.reg_start_addr = MB_START_0;
    reg_tmp.reg_end_addr = MB_END_0;
    if(add_reg_type(reg_tmp) < 0){
        DBG_LEVEL_APP(APP_ERR, NM_NAME, "error add 0 cell");
        return -1;
    }

    reg_tmp.type = MB_ADDR_1;
    reg_tmp.reg_start_addr = MB_START_1;
    reg_tmp.reg_end_addr = MB_END_1;
    if(add_reg_type(reg_tmp) < 0){
        DBG_LEVEL_APP(APP_ERR, NM_NAME, "error add 1 cell");
        return -1;
    }

    reg_tmp.type = MB_ADDR_3;
    reg_tmp.reg_start_addr = MB_START_3;
    reg_tmp.reg_end_addr = MB_END_3;
    if(add_reg_type(reg_tmp) < 0){
        DBG_LEVEL_APP(APP_ERR, NM_NAME, "error add 3 cell");
        return -1;
    }

    reg_tmp.type = MB_ADDR_4;
    reg_tmp.reg_start_addr = MB_START_4;
    reg_tmp.reg_end_addr = MB_END_4;
    if(add_reg_type(reg_tmp) < 0){
        DBG_LEVEL_APP(APP_ERR, NM_NAME, "error add 4 cell");
        return -1;
    }

    return 0;
}


int mb_addr_translate(int mdbs_addr, char *c)
{
    if(mdbs_addr>=MB_START_0 && mdbs_addr<=MB_END_0)
    {
        *c = MB_ADDR_0;
    }
    else if(mdbs_addr>=MB_START_1 && mdbs_addr<=MB_END_1)
    {
        *c = MB_ADDR_1;
    }
    else if(mdbs_addr>=MB_START_3 && mdbs_addr<=MB_END_3)
    {
        *c = MB_ADDR_3;
    }
    else if(mdbs_addr>=MB_START_4 && mdbs_addr<=MB_END_4)
    {
        *c = MB_ADDR_4;
    }
    else
    {
        *c = MB_ADDR_E;  //±íÊ¾ERROR³ö´í
        DBG_LEVEL_APP(APP_ERR, "mb", "error mdbs_addr");
        return -1;
    }

    return mdbs_addr;
}
