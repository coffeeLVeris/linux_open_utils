/***********************************************************************************
Copy right:     2016-2026 USR IOT Tech.
Author:         焦岳
Version:        V1.0
Date:           2019-05
Description:    FX协议读写封装
***********************************************************************************/

#include "plc_fx_rw.h"
#include "algo_common.h"
#include "debug_print.h"
#include "byte_array.h"
#include "uart.h"

#define SEND_SIZE 11  //发送总长度
#define MAX_READ 128  //plc一次可读取最大字节数

/******************************读操作开始***********************************/
/**
 * @brief fx_plc_read FX协议PLC读取
 * @param type   寄存器类型
 * @param start  起始地址，这里是查询元件的相对地址，比如要查X5，那么这个值为5，M8000，这个值为8000
 * @param number 查询个数
 * @param buf    接收缓冲区:需要自行清0，对于多字节元件，采用是小端模式。
 * @param data_type  数据类型
 * @return 实际读到的字节数，失败返回-1
 */
int fx_plc_read(char type, int start, int number, char data_type, unsigned char *buf)
{
    int i;
    //send字符串长度千万不能取SEND_SIZE，会造成sprintf越界
    unsigned char send[20]={0x02,0x30,0xff,0xff,0xff,0xff,0xff,0xff,0x03,0xff,0xff};
    char receive[MAX_READ + 4] = {0};  //最大接收+头尾和校验
    unsigned char tmp_buf[MAX_READ];
    int start_reg = 0;
    int end, end_reg = 0;
    int  num_bytes = 0;
    int start_limit, end_limit;
    unsigned int sum;

    DBG_LEVEL_STD(APP_DEBUG, FX_RW_DBSW, FX_RW_NAME, "fx read type = %c,start = %d,number = %d",\
                  type, start, number);

    switch(type)
    {
    case 'S':
        start_limit = START_S;
        end_limit = END_S;
        break;
    case 'X':
        start_limit = START_X;
        end_limit = END_X;
        break;
    case 'Y':
        start_limit = START_Y;
        end_limit = END_Y;
        break;
    case 'T':
        if(data_type == REG_BIT){  //开关型T0-255
            start_limit = START_T;
            end_limit = END_T;
        }else if(data_type == REG_WORD){  //数值型T0-255
            start_limit = START_t;
            end_limit = END_t;
        }
        break;
    case 'M':
        if(start < 8000){  //M0-M1023
            start_limit = START_M;
            end_limit = END_M;
        }else{  //M8000-M8255
            start_limit = START_M8000;
            end_limit = END_M8000;
            start -= 8000;
        }
        break;
    case 'C':
        if(data_type == REG_BIT){  //开关型C0-255
            start_limit = START_C;
            end_limit = END_C;
        }else if(data_type == REG_WORD){  //数值型C0-C199
            start_limit = START_c;
            end_limit = END_c;
        }else if(data_type == REG_DWORD){  //数值型C200-255
            start_limit = START_c200;
            end_limit = END_c200;
            start -= 200;
        }
        break;
    case 'D':
        if(start < 8000){  //D0-511
            start_limit = START_D;
            end_limit = END_D;
        }else{  //D8000-8255
            start_limit = START_D8000;
            end_limit = END_D8000;
            start -= 8000;
        }
        break;
    default:
        DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "not support reg type");
        return -1;
        break;
    }

    //计算结束地址
    end = start + number - 1;  //start这里已经是10进制，所以不需要再转换

    if(data_type == REG_BIT){  //如果是位寄存器类型
        start_reg = start / 8;  //转成8进制后直接除以8就是地址标号
        end_reg = end / 8;
    }else if(data_type == REG_WORD){  //双字节类型
        start_reg = start * 2;  //比如D寄存器，start=1时候，start_reg应该=1000+2
        end_reg = end * 2;
    }else if(data_type == REG_DWORD){  //四字节类型
        start_reg = start * 4;  //比如C200寄存器，start=1时候，start_reg应该=1000+4
        end_reg = end * 4;
    }else{
        DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "error cell mode");
        return -1;
    }

    start_reg += start_limit; //加上起始地址
    end_reg += start_limit; //加上起始地址

    DBG_LEVEL_STD(APP_DEBUG, FX_RW_DBSW, FX_RW_NAME, "start_reg = 0x%04X", start_reg);
    if(start_reg < 0)
    {
        DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "start_reg calc error");
    }

    if(end_reg > end_limit){
        DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, \
                      "error end_reg reg,end_reg = 0x%04X,end_limit = 0x%04X", end_reg, end_limit);
    }

    //2.计算需要读取的字节数
    if(data_type == REG_BIT){  //如果是位寄存器类型
        num_bytes = end_reg - start_reg + 1;
    }else if(data_type == REG_WORD){  //双字节类型
        num_bytes = number * 2;
    }else if(data_type == REG_DWORD){  //四字节类型
        num_bytes = number * 4;
    }else{
        DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "error cell mode");
        return -1;
    }

    DBG_LEVEL_STD(APP_DEBUG, FX_RW_DBSW, FX_RW_NAME, "num_bytes = 0x%02X", num_bytes);

    if(num_bytes<=0 || num_bytes>MAX_READ){  //最多一次读取64字节
        DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "too large num_bytes");
        return -1;
    }

    //3.组合FX协议
    //把寄存器地址和字节数加入
    sprintf((char *)(send + 2), "%04X%02X", start_reg, num_bytes);
    send[8] = 0x03;  //'\0'去掉

    //计算checksum并加入
    sum = calc_chesum(send, 1, SEND_SIZE - 3);
    sprintf((char *)(send + SEND_SIZE - 2), "%02X", sum);

    //到此发给PLC的全部组合完毕，打印出来看看
    DBG_LEVEL_STD(APP_DEBUG, FX_RW_DBSW, FX_RW_NAME, "fx send to PLC len = %d", SEND_SIZE);
    for(i=0; i<SEND_SIZE; i++)
    {
        DBG_PRINT(APP_DEBUG FX_RW_DBSW "%02X ", send[i]);
    }
    DBG_PRINT(APP_DEBUG FX_RW_DBSW "\n");

    //这一块还缺数据缓冲层封装
    //4.发送给PLC并查询

    uart_write(send, SEND_SIZE);
    int recv_len = uart_read(receive, MAX_READ + 4);

    //这里缺返回数据校验
    if(recv_len-4 != num_bytes*2){  //去掉头尾和校验，两个字节表示实际一个字节
        DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "fx read from actual uart len = %d", recv_len);
        for(i=0; i<recv_len; i++)
        {
            DBG_PRINT(APP_ERR FX_RW_DBSW "%02X ", receive[i]);
        }
        DBG_PRINT(APP_ERR FX_RW_DBSW "\n");
        DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "fx read num bytes error");
        return -1;
    }

    DBG_LEVEL_STD(APP_DEBUG, FX_RW_DBSW, FX_RW_NAME, "fx read from actual uart len = %d", recv_len);
    for(i=0; i<recv_len; i++)
    {
        DBG_PRINT(APP_DEBUG FX_RW_DBSW "%02X ", receive[i]);
    }
    DBG_PRINT(APP_DEBUG FX_RW_DBSW "\n");

    //转换并先放到临时缓冲区
    memset(tmp_buf, 0, MAX_READ);
    if(str2hex(tmp_buf, receive+1, num_bytes*2) < 0)
    {
        DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "str2hex error, recv data error");
    }
    if(data_type == REG_BIT){
        //做移位，buf最后存的就是真实位数据
        get_bit(buf, tmp_buf, start%8, number);
    }else if(data_type == REG_WORD || data_type == REG_DWORD){  //双字节类型直接拷贝即可
        memcpy(buf, tmp_buf, num_bytes);
    }else{
        DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "error cell mode");
        return -1;
    }

    DBG_LEVEL_STD(APP_DEBUG, FX_RW_DBSW, FX_RW_NAME, "fx read from PLC final result bytes = %d", num_bytes);
    for(i=0; i<num_bytes; i++)
    {
        DBG_PRINT(APP_DEBUG FX_RW_DBSW "%02X ", buf[i]);
    }
    DBG_PRINT(APP_DEBUG FX_RW_DBSW "\n");

    return num_bytes;
}

/**
 * @brief fx_plc_write FX协议PLC写入
 * @param type   寄存器类型
 * @param start  起始地址，这里是查询元件的相对地址，比如要查X5，那么这个值为5
 * @param buf    发送缓冲区
 * @return
 */
static int fx_plc_write(char type, int start, int number, char data_type, unsigned char *buf)
{
    int i;
    //jiaoyue 2019.1.30 新框架和以前程序各种BUG，先这样处理，后期重写，各种强转导致
    int val = (buf[1]<<8) | buf[0];
    number = 1; //现只支持写一个，预留

    //send字符串长度千万不能取SEND_SIZE，会造成sprintf越界
    unsigned char send_read[20]={0x02,0x30,0xff,0xff,0xff,0xff,0xff,0xff,0x03,0xff,0xff};
    unsigned char send_write[64]={0x02,0x31,0xff,0xff,0xff,0xff,0xff,0xff};
    unsigned char receive[MAX_READ + 4] = {0};  //最大接收+头尾和校验
    int start_reg = 0;
    int start_limit;
    unsigned int sum;
    int send_len = -1;
    unsigned char tmp_val = 0;

    switch(type)
    {
    case 'S':
        start_limit = START_S;
        break;
    case 'X':
        start_limit = START_X;
        break;
    case 'Y':
        start_limit = START_Y;
        break;
    case 'T':
        if(data_type == REG_BIT){  //开关型T0-255
            start_limit = START_T;
        }else if(data_type == REG_WORD){  //数值型T0-255
            start_limit = START_t;
        }
        break;
    case 'M':
        if(start < 8000){  //M0-M1023
            start_limit = START_M;
        }else{  //M8000-M8255
            start_limit = START_M8000;
            start -= 8000;
        }
        break;
    case 'C':
        start_limit = START_C;
        break;
    case 't':
        start_limit = START_t;
        break;
    case 'c':
        if(data_type == REG_BIT){  //开关型C0-255
            start_limit = START_C;
        }else if(data_type == REG_WORD){  //数值型C0-C199
            start_limit = START_c;
        }else if(data_type == REG_DWORD){  //数值型C200-255
            //这个段不支持写
            DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "c200 not support write");
        }
        break;
    case 'D':
        if(start < 8000){  //D0-511
            start_limit = START_D;
        }else{  //D8000-8255
            start_limit = START_D8000;
            start -= 8000;
        }
        break;
    default:
        DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "not support cell type");
        return -1;
        break;
    }

    if(data_type == REG_BIT){  //如果是位寄存器类型
        start_reg = start / 8;  //转成8进制后直接除以8就是地址标号
    }else if(data_type == REG_WORD){  //双字节类型
        start_reg = start * 2;  //比如D寄存器，start=1时候，start_reg应该=1000+2
    }else{
        DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "error cell mode");
        return -1;
    }

    start_reg += start_limit; //加上起始地址

    DBG_LEVEL_STD(APP_DEBUG, FX_RW_DBSW, FX_RW_NAME, "start_reg = 0x%04X", start_reg);
    if(start_reg < 0)
    {
        DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "start_reg calc error");
    }

    //如果是位寄存器，需要先读后写，固定一个字节
    if(data_type == REG_BIT)
    {
        sprintf((char *)(send_read + 2), "%04X%02X", start_reg, 1);
        send_read[8] = 0x03;  //'\0'去掉

        //计算checksum并加入
        sum = calc_chesum(send_read, 1, SEND_SIZE - 3);
        sprintf((char *)(send_read + SEND_SIZE - 2), "%02X", sum);

        //到此发给PLC的全部组合完毕，打印出来看看
        DBG_LEVEL_STD(APP_DEBUG, FX_RW_DBSW, FX_RW_NAME, "fx send_read to PLC len = %d", SEND_SIZE);
        for(i=0; i<SEND_SIZE; i++)
        {
            DBG_PRINT(APP_DEBUG FX_RW_DBSW "%02X ", send_read[i]);
        }
        DBG_PRINT(APP_DEBUG FX_RW_DBSW "\n");

        //4.发送给PLC并查询
        uart_write(send_read, SEND_SIZE);
        int recv_len = uart_read(receive, MAX_READ + 4);

        DBG_LEVEL_STD(APP_DEBUG, FX_RW_DBSW, FX_RW_NAME, "fx read from actual uart len = %d", recv_len);
        for(i=0; i<recv_len; i++)
        {
            DBG_PRINT(APP_DEBUG FX_RW_DBSW "%02X ", receive[i]);
        }
        DBG_PRINT(APP_DEBUG FX_RW_DBSW "\n");

        //转换并先放到临时缓冲区
        if(str2hex(&tmp_val, receive+1, 2) < 0)
        {
            DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "str2hex error, recv data error");
        }

        //到这里tmp_val中存的就是位寄存器所在字节的数据
        if(val == 1)
        {
            tmp_val |= (1 << (start % 8));
        }
        else
        {
            tmp_val &= (~(1 << (start % 8)));
        }

        DBG_LEVEL_STD(APP_INFO, FX_RW_DBSW, FX_RW_NAME, "new val = 0x%02X", tmp_val);
        sprintf((char *)(send_write + 1),"1%04X01%02X", start_reg, tmp_val);
        send_write[10] = 0x3;

        //计算校验
        sum = 0;
        for(i=1; i<11; i++)
        {
            sum += send_write[i];
        }

        sum &= 0xff;
        sprintf((char *)(send_write + 11), "%02X", sum);

        send_len = 13;
    }
    else if(data_type == REG_WORD)
    {
        //双字节寄存器固定写入两个字节
        sprintf((char *)(send_write + 1), "1%04X02%02X%02X", start_reg, buf[0], buf[1]);
        send_write[12] = 0x03;
        sum = 0;
        for(i=1; i<13; i++)
        {
            sum += send_write[i];
        }
        sum &= 0xff;
        sprintf((char *)(send_write + 13), "%02X", sum);

        send_len = 15;
    }
    else
    {
        DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "error cell mode");
    }

    //发送写命令
    DBG_LEVEL_STD(APP_DEBUG, FX_RW_DBSW, FX_RW_NAME, "fx write to plc = %d", send_len);
    for(i=0; i<send_len; i++)
    {
        DBG_PRINT(APP_DEBUG FX_RW_DBSW "%02X ", send_write[i]);
    }
    DBG_PRINT(APP_DEBUG FX_RW_DBSW "\n");

    uart_write(send_write, send_len);
    uart_read(receive, MAX_READ + 4);

    if(receive[0] == FX_ACK){
        DBG_LEVEL_STD(APP_INFO, FX_RW_DBSW, FX_RW_NAME, "write success");
    }else if(receive[0] == FX_NAK){
        DBG_LEVEL_STD(APP_ERR, FX_RW_DBSW, FX_RW_NAME, "write error");
        return -1;
    }else{
        DBG_LEVEL_STD(APP_INFO, FX_RW_DBSW, FX_RW_NAME, "write unknow state");
        return -1;
    }

    return 0;
}
