#ifndef _PLC_MB_GROUP_H_
#define _PLC_MB_GROUP_H_

#include "node_group.h"
#include "cloud_node_opt.h"

int node_merge_init_modbus();
int mb_addr_translate(int mdbs_addr, char *c);

#endif


