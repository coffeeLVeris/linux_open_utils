/**
 ** @author:   浓咖啡
 ** @date:	   2019.5.28
 ** @brief:    协议读写操作公共部分
 */
 
#ifndef __PLC_RW_PUB_H
#define __PLC_RW_PUB_H

#include "debug_print.h"

enum REG_T{
    REG_BIT = 0,  //位寄存器，线圈
    REG_BYTE,  //单字节型
    REG_WORD,  //双字节型
    REG_DWORD, //四字节型
};

#define PLC_PRO_DBG MODULE_DBG_ON
#define PLC_PRO_NAME "plc_pro"

#endif  //__PLC_RW_PUB_H
