/**
 ** @author:   焦岳
 ** @date:	   2019.1.3
 ** @brief:    MODBUS测试程序
 */
 
#ifndef __PLC_MODBUS_TEST_H
#define __PLC_MODBUS_TEST_H

int test_mb_read(int sid, int reg_start_addr, int reg_len, int type);

#define MB_START_0      1
#define MB_END_0        9999

#define MB_START_1      10001
#define MB_END_1        19999

#define MB_START_3      30001
#define MB_END_3        39999

#define MB_START_4      40001
#define MB_END_4        49999


#endif  //__PLC_MODBUS_TEST_H
