/***********************************************************************************
Copy right:	2016-2026 USR IOT Tech.
Author:			王斌
Version:		V1.0
Date:			2019-01
Description:	modbus协议读写操作
***********************************************************************************/

#ifndef _PLC_MB_WR_H
#define _PLC_MB_WR_H


#include "plc_rw_pub.h"
#include "debug_print.h"

#define MB_ADDR_E 'E'  //错误

#define MB_ADDR_0 '0'
#define MB_ADDR_1 '1'
#define MB_ADDR_3 '3'
#define MB_ADDR_4 '4'

#define MB_START_0      1
#define MB_END_0        9999

#define MB_START_1      10001
#define MB_END_1        19999

#define MB_START_3      30001
#define MB_END_3        39999

#define MB_START_4      40001
#define MB_END_4        49999

#define SEND_2_PLC(buff,len)    send_2_plc(buff,len)
#define RECV_4_PLC(buff,len)    recv_4_plc(buff,len)
#define WAIT_4_PLC(time)        wait_4_plc(time)

extern pthread_mutex_t uart_msg_lock;

int modbus_plc_read(int sid, int reg_start_addr, int reg_len, int reg_type, char *buf);

//调试开关
#define MODBUS_PLC_DBG MODULE_DBG_ON
#define MODBUS_PLC_NAME "modbus_plc"

#endif
