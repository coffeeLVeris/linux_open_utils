#include "uart.h"
#include "plc_mb_rw.h"
#include "algo_common.h"
#include "byte_array.h"
#include "plc_poll_common.h"
#include "Crc_Calc.h"

#define MODBUS_MAX_READ 256


int modbus_plc_read(int sid, int reg_start_addr, int reg_len, int reg_type, char *buf)
{
    int i;
    uint8_t cmd_buf[8];
    uint8_t rsp_buf[MODBUS_MAX_READ];
    int cmd_len, rsp_len;
    uint16_t crc_ret;
    int addr;

    if ((sid < 0) || (sid > 255))
    {
        DBG_LEVEL_STD(APP_ERR, MODBUS_PLC_DBG, MODBUS_PLC_NAME, "sid error");
        return -1;
    }

    if ((reg_start_addr < 0) || (reg_start_addr > 65535))
    {
        DBG_LEVEL_STD(APP_ERR, MODBUS_PLC_DBG, MODBUS_PLC_NAME, "reg_start_addr error");
        return -1;
    }

    if ((reg_len < 0) || (reg_len > 65535))
    {
        DBG_LEVEL_STD(APP_ERR, MODBUS_PLC_DBG, MODBUS_PLC_NAME, "reg_len error");
        return -1;
    }


    cmd_buf[0] = (uint8_t)sid;

    if (reg_start_addr >= MB_START_0 && reg_start_addr <= MB_END_0) //ÏßÈ¦¼Ä´æÆ÷
    {
        cmd_buf[1] = 0x01;
    }
    else if (reg_start_addr >= MB_START_1 && reg_start_addr <= MB_END_1) //ÀëÉ¢¼Ä´æÆ÷
    {
        cmd_buf[1] = 0x02;
    }
    else if (reg_start_addr >= MB_START_4 && reg_start_addr <= MB_END_4) //±£³Ö¼Ä´æÆ÷
    {
        cmd_buf[1] = 0x03;
    }
    else if (reg_start_addr >= MB_START_3 && reg_start_addr <= MB_END_3) //ÊäÈë¼Ä´æÆ÷
    {
        cmd_buf[1] = 0x04;
    }
    else //µØÖ·´íÎó
    {
        return -1;
    }


    switch(reg_type)
    {
        case 2:  //Ö»Ö§³Ö0 ºÍ 5
        case 3:
        case 4:
        case 6:
        case 8:
        case 10:
        {
            reg_len *= 2;
            break;
        }

        default:
        {
            break;
        }
    }

    //¼ÆËãµØÖ·ºÍ³¤¶È
    cmd_len = 8;
    addr = reg_start_addr % 10000 - 1;
    cmd_buf[2] = (uint8_t)((uint16_t)addr >> 8);
    cmd_buf[3] = (uint8_t)((uint16_t)addr & 0x00FF);
    cmd_buf[4] = (uint8_t)((uint16_t)reg_len >> 8);
    cmd_buf[5] = (uint8_t)((uint16_t)reg_len & 0x00FF);

    //¼ÆËãCRC
    crc_ret = GetCRC16(cmd_buf, cmd_len-2);
    cmd_buf[6] = (uint8_t)(crc_ret >> 8);
    cmd_buf[7] = (uint8_t)(crc_ret & 0x00FF);

    DBG_LEVEL_STD(APP_DEBUG, MODBUS_PLC_DBG, MODBUS_PLC_NAME, "mb send to PLC len = %d", cmd_len);
    for(i=0; i<cmd_len; i++)
    {
        DBG_PRINT(APP_DEBUG MODBUS_PLC_DBG "%02X ", cmd_buf[i]);
    }
    DBG_PRINT(APP_DEBUG MODBUS_PLC_DBG "\n");

    int ret = uart_write(cmd_buf, cmd_len);
    if(ret < 0){
        DBG_LEVEL_STD(APP_INFO, POLL_DBSW, POLL_NAME, "write err");
        return -1;
    }

    usleep(2000);
    rsp_len = uart_read(rsp_buf, 256);

    DBG_LEVEL_STD(APP_DEBUG, MODBUS_PLC_DBG, MODBUS_PLC_NAME, "recv from plc len = %d", rsp_len);
    for(i=0; i<rsp_len; i++)
    {
        DBG_PRINT(APP_DEBUG MODBUS_PLC_DBG "%02X ", rsp_buf[i]);
    }
    DBG_PRINT(APP_DEBUG MODBUS_PLC_DBG "\n");

    if (rsp_len < 5 || rsp_buf[1] != cmd_buf[1])
    {
        return -1;
    }

    if(reg_type != 5){ //²»ÊÇÎ»¼Ä´æÆ÷ÀàÐÍ£¬ÅÐ¶ÏÏÂ×Ö½ÚÊý
        if(reg_len * 2 != rsp_buf[2]){
            DBG_LEVEL_STD(APP_ERR, MODBUS_PLC_DBG, MODBUS_PLC_NAME, "read err len");
            return -1;
        }
    }

    if(GetCRC16(rsp_buf, rsp_len) != 0)
    {
        DBG_LEVEL_STD(APP_ERR, MODBUS_PLC_DBG, MODBUS_PLC_NAME, "CRC error");
        return -1;
    }

    memcpy(buf, rsp_buf+3, rsp_buf[2]);

    int len = (int)rsp_buf[2];
    return len;
}
