/***********************************************************************************
Copy right:	2016-2026 USR IOT Tech.
Author:			焦岳
Version:		V1.0
Date:			2019-05
Description:	FX读写方法封装
***********************************************************************************/

#ifndef _PLC_FX_RW_H_
#define _PLC_FX_RW_H_

#include "plc_rw_pub.h"

#define FX_REG_ERR  'E'

#define S_REG_PIN   'S'
#define Y_REG_PIN   'Y'
#define T_REG_PIN   'T'
#define C_REG_PIN   'C'
#define M_REG_PIN   'M'
#define M8000_REG_PIN   'm' //M8000
#define X_REG_PIN   'X'

#define D_REG_VEL   'D'
#define D8000_REG_VEL   'd'
#define T_REG_VEL   't'
#define C_REG_VEL   'c'
#define C200_REG_VEL   'e'  //这个表示C200之后的，因为已经有了c，用e表示吧

#define START_S 0x0000
#define END_S 	0x007C

#define START_X 0x0080
#define END_X 	0x008F

#define START_Y 0x00A0
#define END_Y 	0x13AF

//开关型
#define START_T 0x00C0
#define END_T 	0x00DF

//M寄存器有两个段
#define START_M  0x0100
#define END_M 	 0x01FF
#define START_M8000  0x01E0
#define END_M8000 	 0x01FF

#define START_C 0x01C0
#define END_C 	0x01DF

//数值型
#define START_t 0x0800
#define END_t 	0x09FF

#define START_c 0x0A00
#define END_c 	0x0B8F
#define START_c200 0x0C00
#define END_c200 	0x0CDF

#define START_D  0x1000
#define END_D 	 0x13FF
#define START_D8000  0x0E00
#define END_D8000 	 0x0FFF

//写回复成功和失败定义
#define FX_ACK 0x06
#define FX_NAK 	0x15

//调试开关
#define FX_RW_DBSW MODULE_DBG_ON
#define FX_RW_NAME "fx_rw"

int fx_plc_read(char type, int startreg, int number , char data_type, unsigned char *buf);

#endif
