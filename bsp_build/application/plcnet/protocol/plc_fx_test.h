/**
 ** @author:   焦岳
 ** @date:	   2019.1.3
 ** @brief:    FX测试程序
 */
 
#ifndef __PLC_FX_TEST_H
#define __PLC_FX_TEST_H

int test_fx_read();

#endif  //__PLC_FX_TEST_H
