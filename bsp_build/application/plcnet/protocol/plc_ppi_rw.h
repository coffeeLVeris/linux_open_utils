/***********************************************************************************
Copy right:     2016-2026 USR IOT Tech.
Author:         焦岳
Version:        V1.0
Date:           2019-07
Description:    ppi协议读写
***********************************************************************************/

#ifndef _PLC_PPI_WR_H
#define _PLC_PPI_WR_H

#include "plc_rw_pub.h"
#include "debug_print.h"

#define PPI_I_REG    'I'	//输入
#define PPI_Q_REG    'Q'	//输出
#define PPI_M_REG    'M'	//位
#define PPI_AI_REG	'A'	//模拟输入
#define PPI_AQ_REG  'N'	//模拟输出
#define PPI_T_REG    'T'	//定时器
#define PPI_C_REG    'C'	//计数器
#define PPI_HC_REG   'H'	//高速计数器
#define PPI_SM_REG   'S'	//特殊寄存器
#define PPI_V_REG    'V'	//变量

int ppi_plc_read(char sid, char reg_type, int start, int number, char data_type, unsigned char *buf);
int ppi_plc_write(char sid, char reg_type, int start, int number, char data_type, const void *data);

#endif



