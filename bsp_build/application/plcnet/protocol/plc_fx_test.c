/**
 ** @author:   焦岳
 ** @date:	   2019.1.3
 ** @brief:    FX测试程序
 */
 
#include "plc_fx_test.h"
#include "uart.h"
#include "debug_print.h"

#define SEND_SIZE 11

int test_fx_read()
{
    int nbytes;
    int i = 0;
    char buf[64];


    int start_reg = 0x1000;
    int num_bytes = 2;
    //往串口写PLC查询指令
    unsigned char send[20]={0x02,0x30,0xff,0xff,0xff,0xff,0xff,0xff,0x03,0xff,0xff};
    sprintf((char *)(send + 2), "%04X%02X", start_reg, num_bytes);
    send[8] = 0x03;

    unsigned int sum = 0;
    for(i=1; i<SEND_SIZE-2; i++){
        sum += (unsigned char)send[i];
    }
    sum &= 0xff;
    sprintf((char *)(send + SEND_SIZE - 2), "%02X", sum);

    DBG_LEVEL_STD(APP_INFO, COMMON_DBSW, COMMON_DBNM, "send nbytes = %d", SEND_SIZE);
    for(i=0; i<SEND_SIZE; i++)
    {
        DBG_PRINT(APP_INFO COMMON_DBSW "%02X ", send[i]);
    }
    DBG_PRINT(APP_INFO COMMON_DBSW "\n");

    int ret = uart_write(send, SEND_SIZE);
    if(ret < 0){
        perror("write err");
        return -1;
    }
    nbytes = uart_read(buf, 64);

    DBG_LEVEL_STD(APP_INFO, COMMON_DBSW, COMMON_DBNM, "recv nbytes = %d", nbytes);
    for(i=0; i<nbytes; i++)
    {
        DBG_PRINT(APP_INFO COMMON_DBSW "%02X ", buf[i]);
    }
    DBG_PRINT(APP_INFO COMMON_DBSW "\n");

    return 0;
}

