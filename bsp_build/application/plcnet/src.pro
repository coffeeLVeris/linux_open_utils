TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += algorithm core lib protocol cloud platform public tools

SOURCES += main.c \ 
    algorithm/algo_common.c \
    cloud/cloud_node_opt.c \
    core/node_group.c \
    core/plc_poll_common.c \
    lib/cJSON.c \
    lib/Crc_Calc.c \
    lib/mt_timer.c \
    platform/gpio.c \
    platform/platform.c \
    platform/uart.c \
    protocol/plc_mb_group.c \
    protocol/plc_mb_rw.c \
    protocol/plc_modbus_test.c \
    tools/byte_array.c \
    tools/debug_print.c \
    tools/file_opt.c \
    protocol/plc_ppi_rw.c \
    protocol/plc_fx_rw.c \
    protocol/plc_fx_test.c

HEADERS += \ 
    algorithm/algo_common.h \
    cloud/cloud_node_opt.h \
    core/node_group.h \
    core/plc_poll_common.h \
    lib/cJSON.h \
    lib/Crc_Calc.h \
    lib/mt_timer.h \
    lib/uthash.h \
    platform/gpio.h \
    platform/gpio_define.h \
    platform/platform.h \
    platform/uart.h \
    protocol/plc_mb_group.h \
    protocol/plc_mb_rw.h \
    protocol/plc_modbus_test.h \
    protocol/plc_rw_pub.h \
    public/pub_define.h \
    public/public.h \
    tools/byte_array.h \
    tools/debug_print.h \
    tools/file_opt.h \
    tools/list.h \
    protocol/plc_ppi_rw.h \
    protocol/plc_fx_rw.h \
    protocol/plc_fx_test.h
