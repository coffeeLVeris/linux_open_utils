/**
 ** @author:   焦岳
 ** @date:	   2019.1.3
 ** @brief:    串口模块
 */
 
#ifndef __UART_H
#define __UART_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>

void uart_init(void);
ssize_t uart_read(void *buf, size_t count);
ssize_t uart_write(const void *buf, size_t count);
int get_fd();

#endif
