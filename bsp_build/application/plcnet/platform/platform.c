/***********************************************************************************
Copy right:     2016-2026 USR IOT Tech.
Author:         焦岳
Version:        V1.0
Date:           2019-05
Description:    平台相关初始化
***********************************************************************************/

#include "gpio.h"

void usr_led_init()
{
    Gpio_Init( PA5, GPIO_MODE_OUT );//work
    Gpio_Init( PA9, GPIO_MODE_OUT );//work

    Gpio_Init( PC7, GPIO_MODE_OUT );//net

    Gpio_Init( PC9, GPIO_MODE_OUT );//net_status
    Gpio_Init( PE9, GPIO_MODE_OUT );//net_mode

    Gpio_Init( PE8, GPIO_MODE_OUT );//linkA

    Gpio_Init( PD18, GPIO_MODE_OUT );//linkB

    Gpio_Init( PP19, GPIO_MODE_OUT );//dog

    Gpio_Init( PA11, GPIO_MODE_OUT );

    Gpio_Init( PB11, GPIO_MODE_IN);//reload
}

void usr_dog_feed(void)
{
    if(Gpio_GetLevel(PP19))
    {
        Gpio_Output( PP19, GPIO_LEVEL_L );
    }
    else if(!Gpio_GetLevel(PP19))
    {
        Gpio_Output( PP19, GPIO_LEVEL_H );
    }
}
