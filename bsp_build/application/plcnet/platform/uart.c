/**
 ** @author:   焦岳
 ** @date:	   2019.1.3
 ** @brief:    串口模块
 */

#include "debug_print.h"
#include "uart.h"
#include "public.h"

typedef enum
{
    HAL_FLOW_NONE   =0,
    HAL_FLOW_RTS_CTS,
    HAL_FLOW_RS485,
    HAL_FLOW_9PIN,
}SYS_SET_UART_FLOW;

//define woke mode, AT mode or throughput mode
typedef enum
{
    UART_MODE_AT = 0,
    UART_MODE_DT = 1,
} UART_MODE;


//define uart parameters
typedef enum
{
    UART_CHK_NONE   = 0,
    UART_CHK_ODD    = 1,
    UART_CHK_EVEN   = 2,
    UART_CHK_SPACE  = 3,
    UART_CHK_MARK   = 4,
} UART_PARITY_TYPE;


typedef enum
{
    UART_FLOW_NFC   = 0,
    UART_FLOW_CRTS  = 1,
    UART_FLOW_XON   = 2,
    UART_FLOW_RS485 = 3,
} UART_FLOW_TYPE;


typedef enum
{
    UART_DATA_BITS_5 = 5,
    UART_DATA_BITS_6 = 6,
    UART_DATA_BITS_7 = 7,
    UART_DATA_BITS_8 = 8,
} UART_DATA_BITS;

typedef enum
{
    UART_STOP_BITS_1 = 1,
    UART_STOP_BITS_2 = 2,
} UART_STOP_BITS;

static int fd;
static struct termios newtios,oldtios; //termianal settings

static int uart_set_opt(int fd,uint nspeed,UART_PARITY_TYPE parity,UART_DATA_BITS data,UART_STOP_BITS stop)
{
    fcntl(fd, F_SETFL, 0) ; //设置阻塞

    /*get serial port parnms,save away */
    tcgetattr( fd, &newtios );
    memcpy( &oldtios, &newtios, sizeof(newtios) );

    //configure new values
    cfmakeraw( &newtios );

    newtios.c_iflag |= IGNPAR; /*ignore parity on input */
    newtios.c_oflag &= ~(OPOST | ONLCR | OLCUC | OCRNL | ONOCR | ONLRET | OFILL);
    newtios.c_cflag = CS8 | CLOCAL | CREAD;
    newtios.c_cc[VMIN] = 255;  //c_cc限制，这个值最大为255，这里可能会导致问题
    newtios.c_cc[VTIME] = 1; //内核默认以100ms为单位，这地方需要修改内核

    //data_bits
    switch(data)
    {
    case UART_DATA_BITS_7:
    {
        newtios.c_cflag &= ~CSIZE;
        newtios.c_cflag |= CS7;
        break;
    }

    case UART_DATA_BITS_8:
    {
        newtios.c_cflag &= ~CSIZE;
        newtios.c_cflag |= CS8;
        break;
    }

    default:
    {
        newtios.c_cflag &= ~CSIZE;
        newtios.c_cflag |= CS8;
        break;
    }
    }


    //stop_bits
    switch(stop)
    {
    case UART_STOP_BITS_1:
    {
        newtios.c_cflag &= ~CSTOPB;
        break;
    }

    case UART_STOP_BITS_2:
    {
        newtios.c_cflag |= CSTOPB;
        break;
    }

    default:
    {

        newtios.c_cflag &= ~CSTOPB;
        break;
    }
    }


    //parity
    switch( parity )
    {
    case UART_CHK_NONE:
    {
        newtios.c_cflag &= ~PARENB;
        break;
    }

    case UART_CHK_ODD:
    {
        newtios.c_cflag |= PARENB;
        newtios.c_cflag |= PARODD;
        break;
    }

    case UART_CHK_EVEN:
    {
        newtios.c_cflag |= PARENB;
        newtios.c_cflag &= ~PARODD;
        break;
    }
    default:
    {

        newtios.c_cflag &= ~PARENB;
        break;
    }
    }



    //baud rate
    switch(nspeed)
    {
    case 300:
    {
        cfsetospeed(&newtios,B300);
        cfsetispeed(&newtios,B300);
        break;
    }

    case 600:
    {
        cfsetospeed(&newtios,B600);
        cfsetispeed(&newtios,B600);
        break;
    }

    case 1200:
    {
        cfsetospeed(&newtios,B1200);
        cfsetispeed(&newtios,B1200);
        break;
    }

    case 2400:
    {
        cfsetospeed(&newtios,B2400);
        cfsetispeed(&newtios,B2400);
        break;
    }

    case 4800:
    {
        cfsetospeed(&newtios,B4800);
        cfsetispeed(&newtios,B4800);
        break;
    }

    case 9600:
    {
        cfsetospeed(&newtios,B9600);
        cfsetispeed(&newtios,B9600);
        break;
    }

    case 19200:
    {
        cfsetospeed(&newtios,B19200);
        cfsetispeed(&newtios,B19200);
        break;
    }

    case 38400:
    {
        cfsetospeed(&newtios,B38400);
        cfsetispeed(&newtios,B38400);
        break;
    }

    case 57600:
    {
        cfsetospeed(&newtios,B57600);
        cfsetispeed(&newtios,B57600);
        break;
    }

    case 115200:
    {
        cfsetospeed(&newtios,B115200);
        cfsetispeed(&newtios,B115200);
        break;
    }

    case 230400:
    {
        cfsetospeed(&newtios,B230400);
        cfsetispeed(&newtios,B230400);
        break;
    }

    case 460800:
    {
        cfsetospeed(&newtios,B460800);
        cfsetispeed(&newtios,B460800);
        break;
    }

    case 921600:
    {
        cfsetospeed(&newtios,B921600);
        cfsetispeed(&newtios,B921600);
        break;
    }

    default:
    {
        cfsetospeed(&newtios,B115200);
        cfsetispeed(&newtios,B115200);
        break;
    }
    }

    tcflush( fd, TCIFLUSH );
    tcsetattr( fd, TCSADRAIN, &newtios );

    return 1;
}

#if MACHINE_TYPE == MACHINE_ZX7520
#define DEV_UART0 "/dev/ttyS0"
#elif MACHINE_TYPE == MACHINE_VIRTUAL
#define DEV_UART0 "/dev/ttySAC1"
#endif

void uart_init(void)
{
    fd = open( DEV_UART0, O_RDWR|O_NOCTTY );
    if(fd < 0){
        perror("err open");
    }else{
        printf("init success\n");
    }

    if(uart_set_opt(fd, 9600, UART_CHK_EVEN, UART_DATA_BITS_7, UART_STOP_BITS_1)==0){
        perror("err set opt");
    }
}

ssize_t uart_read(void *buf, size_t count)
{
    return read(fd, buf, count);
}

ssize_t uart_write(const void *buf, size_t count)
{
    return write(fd, buf, count);
}

int get_fd()
{
    return fd;
}


