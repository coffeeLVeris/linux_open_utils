/***********************************************************************************
Copy right:		2015-2025 USR IOT Tech.
Author:			Wangbin
Version:		V1.0
Date:			2015-09
Description:	gpio setting.
***********************************************************************************/
#ifndef	__GPIO_H
#define	__GPIO_H


#ifdef	__GPIO_C
#define	GPIO_EXT
#else
#define	GPIO_EXT extern
#endif


#include "gpio_define.h"


typedef enum GPIO_MODE_ENUM
{
	GPIO_MODE_IN = 0,
	GPIO_MODE_OUT,
} gpioMode_t;


typedef enum GPIO_LV_ENUM
{
	GPIO_LEVEL_E = -1,
	GPIO_LEVEL_L = 0,
	GPIO_LEVEL_H = 1,
} gpioLevel_t;




/******************************************************
Input:	GPIO number
Output:	no
Return:	BOOL, TRUE for successful, FALSE for fail
Description: no
********************************************************/
GPIO_EXT void Gpio_Init( int io_id, gpioMode_t mode );


/******************************************************
Input:	GPIO number
Output:	no
Return:	BOOL, TRUE for successful, FALSE for fail
Description: no
********************************************************/
GPIO_EXT void Gpio_Output( int io_id, gpioLevel_t lv );


/******************************************************
Input:	GPIO number
Output:	no
Return:	BOOL, TRUE for successful, FALSE for fail
Description: no
********************************************************/
GPIO_EXT gpioLevel_t Gpio_GetLevel( int io_id );



#endif
