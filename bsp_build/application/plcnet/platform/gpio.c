/***********************************************************************************
Copy right:	2014-2024 USR IOT Tech.
Author:			Bin Wang
Version:			V1.0
Date:				2014-08
Description:	network thread.
***********************************************************************************/
#define __GPIO_C


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "gpio.h"



//define GPIO check func
#define ASSERT_GPIO_ID(io_id) \
    do \
{ \
    if( io_id < MIN_GPIO_ID \
    || io_id > MAX_GPIO_ID ) \
{ \
    printf( "file:%s, line:%d, gpio id error!\n", __FILE__, __LINE__ ); \
    } \
    } while(0)


#define ASSERT_GPIO_MODE(io_mode) \
    do \
{ \
    if( io_mode != GPIO_MODE_IN \
    && io_mode != GPIO_MODE_OUT ) \
{ \
    printf( "file:%s, line:%d, gpio mode error!\n", __FILE__, __LINE__ ); \
    } \
    } while(0)


#define ASSERT_GPIO_LEVEL(io_lv) \
    do \
{ \
    if( io_lv != GPIO_LEVEL_L \
    && io_lv != GPIO_LEVEL_H ) \
{ \
    printf( "file:%s, line:%d, gpio level error!\n", __FILE__, __LINE__ ); \
    } \
    } while(0)




void Gpio_Init( int io_id, gpioMode_t mode )
{
    char cmd[100];

    ASSERT_GPIO_ID( io_id );
    ASSERT_GPIO_MODE( mode );


    //check if gpio file exist, or create it
    sprintf( cmd, "/sys/class/gpio/gpio%d/direction", io_id );

    if ( access( cmd, 0 ) < 0 )
    {
        sprintf( cmd, "echo %d > /sys/class/gpio/export", io_id );
        system( cmd );
    }


    //set derection
    if ( mode == GPIO_MODE_IN )
    {
        sprintf( cmd, "echo in > /sys/class/gpio/gpio%d/direction", io_id );
    }
    else
    {
        sprintf( cmd, "echo out > /sys/class/gpio/gpio%d/direction", io_id );
    }

    system( cmd );
}


void Gpio_Output( int io_id, gpioLevel_t lv )
{
    char cmd[100];

    ASSERT_GPIO_ID( io_id );
    ASSERT_GPIO_LEVEL( lv );

    if( lv == GPIO_LEVEL_H )
    {
        sprintf( cmd, "echo 1 > /sys/class/gpio/gpio%d/value", io_id );
    }
    else
    {
        sprintf( cmd, "echo 0 > /sys/class/gpio/gpio%d/value", io_id );
    }

    system( cmd );
}


int Gpio_GetLevel( int io_id )
{
    FILE *fp;
    char cmd[100];


    ASSERT_GPIO_ID( io_id );

    sprintf( cmd, "/sys/class/gpio/gpio%d/value", io_id );

    fp = fopen( cmd, "r" );

    if( fp )
    {
        memset( cmd, 0, sizeof(cmd) );
        fgets( cmd, sizeof(cmd), fp );
        fclose( fp );

        if( cmd[0] == '1' )
        {
            return GPIO_LEVEL_H;
        }
        else
        {
            return GPIO_LEVEL_L;
        }
    }

    return GPIO_LEVEL_E;

}




