/**
 ** @author:   焦岳
 ** @date:	   2019.1.3
 ** @brief:    公共定义
 */
 
#ifndef __PUBLIC_H
#define __PUBLIC_H

#include <stdio.h>
#include <string.h>

//系统定义
#define OS_LINUX 1
#define OS_RT    2
//当前系统
#define OS_TYPE OS_LINUX

//平台定义
#define MACHINE_VIRTUAL  1
#define MACHINE_ZX7520   2
//当前平台
#define MACHINE_TYPE MACHINE_VIRTUAL

#endif  //__PUBLIC_H
