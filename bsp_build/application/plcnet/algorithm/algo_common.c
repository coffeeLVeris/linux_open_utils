/***********************************************************************************
Copy right:     2016-2026 USR IOT Tech.
Author:         焦岳
Version:        V1.0
Date:           2019-05
Description:    通用算法模块
***********************************************************************************/

#include "algo_common.h"
#include <assert.h>

/**
 * @brief oct2dec
 * @param oct_str 八进制数据字符串
 * @return 10进制数据
 */
int oct2dec(char *oct_str)
{
    int tmp;
    int r = 0;
    while(*oct_str)
    {
        tmp = *oct_str - '0';
        if(tmp>7 || tmp<0){
            return -1;
        }
        r = r*8 + tmp;
        oct_str++;
    }
    return r;
}

/**
 * @brief 校验和计算
 * @param data
 * @param start
 * @param end
 * @return
 */
uint8 calc_chesum(const uint8 *data, int start, int end)
{
    int i;
    int sum = 0;

    assert(start >= 0 && end >= start);

    for(i=start; i<=end; i++){
        sum += data[i];
    }

    return (uint8)sum & 0xff;
}

/**
 * @brief 校验和计算
 * @param data
 * @param start
 * @param end
 * @return
 */
uint8 calc_bcc(const uint8 *data, int start, int end)
{
    int i;
    int bcc = 0x00;

    assert(start >= 0 && end >= start);

    for(i=start; i<=end; i++){
        bcc ^= data[i];
    }

    return bcc;
}


