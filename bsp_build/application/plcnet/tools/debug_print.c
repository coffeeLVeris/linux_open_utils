/***********************************************************************************
Copy right:     2016-2026 USR IOT Tech.
Author:         焦岳
Version:        V1.0
Date:           2019-05
Description:    打印组件
***********************************************************************************/

#include "debug_print.h"

static FILE *fp_log = NULL;

static struct dbg_parame parame = {DEFAULT_DBG_LIMIT, DEFAULT_LOG_LIMIT, DEFAULT_LOG_CUT_SIZE, "test.log",
                                  false, DEFAULT_LOG_MAX_NUM, "/log_save", "/etc/log_daemon.sh"};

static int init_log_file()
{
    int open_count = 0;

    while((fp_log==NULL) && (++open_count<4)){
        fp_log = fopen(parame.log_file, "a+");
    }

    if(fp_log == NULL){
        return -1;
    }else{
        return 0;
    }
}

void set_dbg_para(struct dbg_parame para)
{
    parame = para;
}

void set_dbg_limit(const char *buf)
{
    if((buf[0] == '<') && (buf[2] == '>')){
        int dbglevel = buf[1] - '0';
        if(dbglevel >= 0 && dbglevel <= 7){
            parame.dbg_limit = dbglevel;
        }
    }
}

int init_dbg()
{
    //首先创建日志文件
    if(init_log_file() < 0){
        printf("serious error:create %s\n", parame.log_file);
    }

    if(parame.start_daemon){
        //启动守护脚本
        if((access(parame.shell_file, F_OK)) < 0){
            printf("serious error:lack %s\n", parame.shell_file);
            return -1;
        }
        char cmd_tmp[256];
        sprintf(cmd_tmp, "%s %s %d &", \
                parame.shell_file, parame.log_path, parame.max_log_num);
        printf("start log_daemon.sh\n");
        system(cmd_tmp);
    }

    return 0;
}

/**
 * @brief 以当前时间生成文件名
 * @param dst
 */
static void get_time_name(char *dst)
{
    char tmp[256];
    time_t time_cur = time(NULL);
    strftime(tmp, sizeof(tmp), "%m%d-%H%M%S.log", localtime(&time_cur));
    strcpy(dst, tmp);
}

int DebugPrintf(const char *format, ...)
{
    char cmd_tmp[256];
    char str_tmp[MAX_BUF_LEN];
    char *tmp;
    va_list arg;
    int len;
    int dbglevel = DEFAULT_DBG_LIMIT;
    
    va_start (arg, format);
    len = vsprintf(str_tmp, format, arg);
    va_end (arg);
    str_tmp[len] = '\0';

    tmp = str_tmp;
    
    //printf("<4><1><5>");
    // 根据打印级别决定是否打印
    if((str_tmp[0] == '<') && (str_tmp[2] == '>')){
        dbglevel = str_tmp[1] - '0';
        if (dbglevel >= 0 && dbglevel <= 9){
            tmp = tmp + 3;
        }else{
            dbglevel = DEFAULT_DBG_LIMIT;
        }
    }

    // 根据模块判断是否打印
    if((str_tmp[3] == '(') && (str_tmp[5] == ')')){
        int on = str_tmp[4] - '0';
        if(on == 1){
            tmp = tmp + 3;
        }else{
            return -1;
        }
    }

    if (dbglevel > parame.dbg_limit){
        return -1;
    }

    char str_final[MAX_BUF_LEN];
    switch (dbglevel) {
    case 5:
        sprintf(str_final, "%s ------- notice\r\n", tmp);
        break;
    case 4:
        sprintf(str_final, "%s ------- warning\r\n", tmp);
        break;
    case 3:
        sprintf(str_final, "%s ------- error\r\n", tmp);
        break;
    case 2:
        sprintf(str_final, "%s ------- critical\r\n", tmp);
        break;
    case 1:
        sprintf(str_final, "%s ------- alert\r\n", tmp);
        break;
    case 0:
        sprintf(str_final, "%s ------- emergency\r\n", tmp);
        break;
    default:
        sprintf(str_final, "%s\r\n", tmp);
        break;
    }

    //如果当前级别小于日志级别，那么同时输出到日志文件
    if(dbglevel <= parame.log_limit){
        if(fp_log == NULL){
            printf("please init log file first\n");
            return -1;
        }
        fwrite(str_final, 1, strlen(str_final), fp_log);	//写入到日志文件,暂时不检测是否写入失败问题
        fflush(fp_log);
        if(ftell(fp_log) > parame.log_cut_size){  //如果日志文件大于限定的大小，那么暂存
            char new_name[256];
            get_time_name(new_name); //获取新的文件名
            //把当前日志拷贝到日志缓存目录中，并清空原文件
            sprintf(cmd_tmp, "cp %s %s/%s;cat /dev/null > %s;date > %s", \
                    parame.log_file, parame.log_path, new_name, parame.log_file, parame.log_file);
            system(cmd_tmp);
        }
    }

    printf("%s", str_final);

    return 0;
    
}

int DebugPrint(const char *format, ...)
{
    char cmd_tmp[256];
    char str_tmp[MAX_BUF_LEN];
    char *tmp;
    va_list arg;
    int len;
    int dbglevel = DEFAULT_DBG_LIMIT;

    va_start (arg, format);
    len = vsprintf(str_tmp, format, arg);
    va_end (arg);
    str_tmp[len] = '\0';

    tmp = str_tmp;

    //printf("<4><1><5>");
    // 根据打印级别决定是否打印
    if((str_tmp[0] == '<') && (str_tmp[2] == '>')){
        dbglevel = str_tmp[1] - '0';
        if (dbglevel >= 0 && dbglevel <= 9){
            tmp = tmp + 3;
        }else{
            dbglevel = DEFAULT_DBG_LIMIT;
        }
    }

    // 根据模块判断是否打印
    if((str_tmp[3] == '(') && (str_tmp[5] == ')')){
        int on = str_tmp[4] - '0';
        if(on == 1){
            tmp = tmp + 3;
        }else{
            return -1;
        }
    }

    if (dbglevel > parame.dbg_limit){
        return -1;
    }

    //如果当前级别小于日志级别，那么同时输出到日志文件
    if(dbglevel <= parame.log_limit){
        if(fp_log == NULL){
            printf("please init log file first\n");
            return -1;
        }
        fwrite(tmp, 1, strlen(tmp), fp_log);	//写入到日志文件,暂时不检测是否写入失败问题
        fflush(fp_log);
        if(ftell(fp_log) > parame.log_cut_size){  //如果日志文件大于限定的大小，那么暂存
            char new_name[256];
            get_time_name(new_name); //获取新的文件名
            //把当前日志拷贝到日志缓存目录中，并清空原文件
            sprintf(cmd_tmp, "cp %s %s/%s;cat /dev/null > %s;date > %s", \
                    parame.log_file, parame.log_path, new_name, parame.log_file, parame.log_file);
            system(cmd_tmp);
        }
    }

    printf("%s", tmp);

    return 0;

}

long get_sys_runtime(int type)
{
    struct timespec times = {0, 0};
    long time;

    clock_gettime(CLOCK_MONOTONIC, &times);

    if (1 == type){
        time = times.tv_sec;
    }else{
        time = times.tv_nsec / 1000000;
    }

    // printf("time = %ld\n", time);
    return time;
}









