/***********************************************************************************
Copy right:     2016-2026 USR IOT Tech.
Author:         焦岳
Version:        V1.0
Date:           2019-05
Description:    字节流处理模块
***********************************************************************************/

#ifndef __BYTE_ARRAY_H
#define __BYTE_ARRAY_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int is_A2F(char ch);
int str2hex(unsigned char *dst, const char *src, int len);

int BIT_ISSET(int n, unsigned const char *buf);
void BIT_SET(int n, unsigned char *buf);
void BIT_ZERO(int n, unsigned char *buf);
void get_bit(unsigned char *dst, const unsigned char *src, int start, int num);

int GET_DVAL(int n, char *buf);
int GET_FVAL(int n, char *buf);
int conver_bos(unsigned char *dst, const unsigned char *src, int len);
int conver_bol(unsigned char *dst, const unsigned char *src, int len);

#endif
