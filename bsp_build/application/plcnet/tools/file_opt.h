/***********************************************************************************
Copy right:     2016-2026 USR IOT Tech.
Author:         焦岳
Version:        V1.0
Date:           2019-05
Description:    文件操作
***********************************************************************************/

#ifndef __FILE_OPT_H
#define __FILE_OPT_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

long FileOpt_GetSize(const char *path);
size_t FileOpt_ReadAll(const char *path, char *ret_buf);

#endif
