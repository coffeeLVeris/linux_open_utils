#ifndef _PLC_POLL_COMMON_H_
#define _PLC_POLL_COMMON_H_

/***********************************************************************************
Copy right:     2016-2026 USR IOT Tech.
Author:         焦岳
Version:        V1.0
Date:           2019-05
Description:    PLC轮询通用策略
***********************************************************************************/

#include "list.h"
#include "debug_print.h"
#include "node_group.h"

#define POLL_DBSW MODULE_DBG_ON
#define POLL_NAME "plc_poll"

enum obj_type{
    ONCE_T = 0,  //只查询一次
    POLL_T, //需要不断轮询
    TIME_T, //定时查询
};

struct plc_poll_obj{
    int type;
    struct group_info *group; //指向所属的组
    struct timespec tm;  //TIME_T类型有
    int (*read_session)(struct group_info *info);  //plc的读会话
    struct list_head list;
};

#define POLL_OBJ_LEN sizeof(struct plc_poll_obj)

void init_poll();
void add_poll_obj(struct plc_poll_obj *obj);
void add_poll_list(struct list_head *obj_list);
void *plc_poll_run(void *arg);

int poll_is_empty();
struct plc_poll_obj *pool_get_node();
void pool_put_node(struct plc_poll_obj *node);

#endif


