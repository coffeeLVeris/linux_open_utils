#ifndef _NODE_GROUP_H_
#define _NODE_GROUP_H_

#include "pub_define.h"
#include "debug_print.h"
#include "list.h"

//结点融合模块
#define NM_DBSW MODULE_DBG_ON
#define NM_NAME "node_merge"

#define MAX_CACHE_LEN 256  //这个值再论

struct group_info{
    unsigned int idx;//分组编号
    char sid;//从机号
    unsigned char vk;//寄存器数值类型
    unsigned char reg_addr_space;//地址间隔，相邻寄存器地址间隔
    unsigned char group_node_count;//节点实际个数,这个暂时没用
    unsigned char group_limit_nc;//分组边界限定节点个数，实际有用是这个
    unsigned int group_start_addr;//分组起始地址
    unsigned int group_end_addr;//分组结束地址
    unsigned int node_mask;//节点位示图，用于标记节点使用情况
    bool data_avl;  //组数据有效标志，如果某次读取失败，这个值会被置为flase，那么不会上报
    unsigned char new_data_buf[MAX_CACHE_LEN];
    unsigned char new_len;
    unsigned char old_data_buf[MAX_CACHE_LEN];
    unsigned char old_len;
    pthread_mutex_t mutex_lock;//用于保护临界资源，互斥临界区代码(保护new_data_buf,old_data_buf,new_len,old_len)
    struct reg_type *parent_reg;
    struct list_head list;
};

struct reg_type{
    char type;  //寄存器类型
    unsigned int reg_start_addr;  //起始地址,使用modbus地址
    unsigned int reg_end_addr;  //结束地址,使用modbus地址
    unsigned char reg_max_count; //一个分组最大节点个数
    int group_count;  //此类型寄存器组总数，不需要赋值
    int (*reg_report_fun)(struct group_info *groupInfo);  //上报接口函数
    int (*set_addr_space)(struct group_info *groupInfo);  //用于确定不同寄存器使用不同vk类型时地址间隔值，间隔这个值时认为地址连续
    int (*register2poll)(struct group_info *groupInfo);  //把读组命令加入到poll层方法
    struct list_head group_list;
    struct list_head list;
};

int node_merge_init();
int add_reg_type(struct reg_type new_reg);


#endif


