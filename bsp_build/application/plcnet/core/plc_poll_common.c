/***********************************************************************************
Copy right:     2016-2026 USR IOT Tech.
Author:         焦岳
Version:        V1.0
Date:           2019-05
Description:    PLC轮询通用策略
***********************************************************************************/

#include "plc_poll_common.h"
#include "uart.h"
#include "mt_timer.h"
#include "debug_print.h"

/**
 * @brief 1.0设计
 * 轮询策略：轮询+单次+定时
 * 轮询优先级相对较低，会一直执行，适合主动查询。单次优先级高，会首先进行，适合云端主动采集。定时是利用定时器加到单次里，适合定时任务。
 * 增加单独内存管理，节点缓冲池
 * 锁处理未做，考虑不加会不会出问题，只有单次链表中会有互斥问题存在。
 */
TIMER_CREATE(pool_timer); //轮询定时器
static struct list_head head_once;
static struct list_head head_poll;
#define MAX_POLL_TIMER 10

static void init_pool();

/**
 * @brief 初始化链表、定时器、锁
 */
void init_poll()
{
    init_pool(); //初始化节点缓冲池
    INIT_LIST_HEAD(&head_once);
    INIT_LIST_HEAD(&head_poll);
    TIMER_INIT(pool_timer, MAX_POLL_TIMER);
}

/**
 * @brief 增加一个节点，只支持单次和轮询节点，因为现在采用定时器有数量限制。
 * @param obj
 */
void add_poll_obj(struct plc_poll_obj *obj)
{
    switch (obj->type) {
    case ONCE_T:
        list_add_tail(&obj->list, &head_once);
        break;
    case POLL_T:
        list_add_tail(&obj->list, &head_poll);
        break;
    case TIME_T:
        //定时器数量有限，不要挨个添加
        DBG_LEVEL_STD(APP_ERR, POLL_DBSW, POLL_NAME, "timer limit");
        break;
    default:
        DBG_LEVEL_STD(APP_ERR, POLL_DBSW, POLL_NAME, "not support obj type");
        break;
    }
}

/**
 * @brief 超时方法
 * @param arg
 */
void timeout_handle(void *arg)
{
    struct list_head *new_list = (struct list_head *)arg;
    list_splice(&head_once, new_list);  //这里把所有的定时节点放到单次轮询链表中去
}

/**
 * @brief 一次添加多个节点，节点为时间节点必须保证定时时间相同
 * @param obj_list
 */
void add_poll_list(struct list_head *obj_list)
{
    struct itimerspec tm_tmp;
    struct list_head *pos;
    struct plc_poll_obj *obj_tmp;

    pos = get_first(obj_list);
    obj_tmp = list_entry(pos, struct plc_poll_obj, list);

    switch (obj_tmp->type) {
    case ONCE_T:
        list_splice(obj_list, &head_once);
        break;
    case POLL_T:
        list_splice(obj_list, &head_poll);
        break;
    case TIME_T:
        tm_tmp.it_value = obj_tmp->tm;
        tm_tmp.it_interval = obj_tmp->tm;
        TIMER_ADD(pool_timer, &tm_tmp, -1, timeout_handle, obj_list, NULL);
        break;
    default:
        DBG_LEVEL_STD(APP_ERR, POLL_DBSW, POLL_NAME, "not support obj type");
        break;
    }
}

/**
 * @brief 开始轮询，供线程使用
 * @param arg
 */
void *plc_poll_run(void *arg)
{
    struct plc_poll_obj *obj_tmp;
    struct list_head *pos;

    while (1) {
        //先轮询只执行一次的
        list_for_each(pos, &head_once){
            obj_tmp = list_entry(pos, struct plc_poll_obj, list);
            obj_tmp->read_session(obj_tmp->group);
            //把这个节点删掉
            list_del(&obj_tmp->list);
        }

        //轮询一直执行的
        list_for_each(pos, &head_poll){
            obj_tmp = list_entry(pos, struct plc_poll_obj, list);
            obj_tmp->read_session(obj_tmp->group);
        }

        sleep(10);
    }
}

/************************************以下是缓冲池，无锁*************************************************/
static int node_num = 0;  //缓冲池结点数
static struct list_head node_pool_head;  //缓冲池头
#define INIT_POOL_COUNT 50  //默认缓冲池节点个数

static void init_pool()
{
    INIT_LIST_HEAD(&node_pool_head);

    int i;
    struct plc_poll_obj *pool_ram = (struct plc_poll_obj *)malloc(POLL_OBJ_LEN * INIT_POOL_COUNT);
    struct plc_poll_obj *tmp = pool_ram;
    if(NULL == pool_ram){
        DBG_LEVEL_STD(APP_EMERG, POLL_DBSW, POLL_NAME, "mem is not enough");
        return;
    }

    for(i=0; i<INIT_POOL_COUNT; i++){
        //DBG_LEVEL_STD(APP_DEBUG, POLL_DBSW, POLL_NAME, "pool addr[%d] = %p", i, tmp);
        memset(tmp, 0, POLL_OBJ_LEN);
        list_add(&tmp->list, &node_pool_head);
        tmp++;
    }
}

int poll_is_empty()
{
    return list_empty(&node_pool_head);
}

struct plc_poll_obj *pool_get_node()
{
    struct list_head *pos;
    struct plc_poll_obj *tmp;

    if (poll_is_empty()){
        //如果缓冲池无节点可用，那么直接申请分配过去，使用完了以后放回即可。
        tmp = (struct plc_poll_obj *)malloc(POLL_OBJ_LEN);
        if(NULL == tmp){
            DBG_LEVEL_STD(APP_EMERG, POLL_DBSW, POLL_NAME, "mem is not enough");
            return NULL;
        }
        memset(tmp, 0, POLL_OBJ_LEN);
    }else{
        pos = get_last(&node_pool_head);
        tmp = list_entry(pos, struct plc_poll_obj, list);

        list_del(pos);
        node_num--;
    }
    return tmp;
}

/**
 * @brief 回收数据仓库删除的结点
 */
void pool_put_node(struct plc_poll_obj *node)
{
    memset(&node, 0, POLL_OBJ_LEN);
    list_add(&node->list, &node_pool_head);
    node_num++;
}
