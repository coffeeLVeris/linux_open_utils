/***********************************************************************************
Copy right:     2016-2026 USR IOT Tech.
Author:         焦岳
Version:        V1.0
Date:           2019-05
Description:    节点合并处理
***********************************************************************************/

#include <pthread.h>
#include "node_group.h"
#include "byte_array.h"
#include "plc_mb_group.h"
#include "algo_common.h"
#include "plc_poll_common.h"

#define ONE_GROUP_MAX_COUNT 32

static int reg_type_count = 0;
static int group_count = 0;
static struct list_head reg_list;

static void group_limit_begin_end()
{
    DBG_PRINT(APP_INFO NM_DBSW "\n");
    DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "will calc new addr and mask");

    int i, shif_tmp;
    int gn_count;
    unsigned int start_addr_tmp;

    struct list_head *pos_reg, *pos_group;
    struct reg_type *tmp_reg;
    struct group_info *tmp_group;

    list_for_each(pos_reg, &reg_list){
        tmp_reg = list_entry(pos_reg, struct reg_type, list);  //找到寄存器类型
        gn_count = tmp_reg->reg_max_count;
        list_for_each(pos_group, &tmp_reg->group_list){
            tmp_group = list_entry(pos_group, struct group_info, list);
            start_addr_tmp = tmp_group->group_start_addr;

            DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "-----------------------------------------------------");
            DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "old start = %d, old end = %d, old mask = 0x%x", \
                          start_addr_tmp, tmp_group->group_end_addr, tmp_group->node_mask);

            //找到第一个有效点，作为首地址
            for(i=0; i<gn_count; i++){
                if(BIT_ISSET(i, (uint8 *)&tmp_group->node_mask)){
                    //DBG_LEVEL_STD(APP_DEBUG, NM_DBSW, NM_NAME,"start i = %d", i);
                    tmp_group->group_start_addr = start_addr_tmp + i * tmp_group->reg_addr_space;
                    break;
                }
            }

            shif_tmp = i; //记录下偏移地址

            //找到最后一个有效点，作为尾地址
            for(i=gn_count-1; i>=0; i--){
                if(BIT_ISSET(i, (uint8 *)&tmp_group->node_mask)){
                    //DBG_LEVEL_STD(APP_DEBUG, NM_DBSW, NM_NAME,"end i = %d", i);
                    tmp_group->group_end_addr = start_addr_tmp + i * tmp_group->reg_addr_space;
                    break;
                }
            }

            //设置新的位图
            tmp_group->node_mask = tmp_group->node_mask >> shif_tmp;
            tmp_group->group_limit_nc = (tmp_group->group_end_addr - tmp_group->group_start_addr) / tmp_group->reg_addr_space + 1;

            //这里缺一个组的最大点位数，供上报时候循环检测用，但是有mask限定，也不会出错，先不加。
            //即尾地址-首地址+1

            DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "new start addr = %d, new end addr = %d, new mask = 0x%x", \
                          tmp_group->group_start_addr, tmp_group->group_end_addr, tmp_group->node_mask);
            DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "reg type = %c, idx = %d, new group_limit_nc = %d", tmp_reg->type, tmp_group->idx, tmp_group->group_limit_nc);

        }
    }
}

static void group_info_show()
{
    printf("\n");
    DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "-----------------------------------------------------");

    struct list_head *pos_reg, *pos_group;
    struct reg_type *tmp_reg;
    struct group_info *tmp_group;

    list_for_each(pos_reg, &reg_list){
        tmp_reg = list_entry(pos_reg, struct reg_type, list);  //找到寄存器类型
        list_for_each(pos_group, &tmp_reg->group_list){
            tmp_group = list_entry(pos_group, struct group_info, list);
            DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "reg type = %c, idx = %d, node limit nc = %d",tmp_reg->type,tmp_group->idx,tmp_group->group_limit_nc);
            DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "start addr = %d, end addr = %d, vk = %d",tmp_group->group_start_addr,tmp_group->group_end_addr,tmp_group->vk);
        }
    }

    printf("\n");
    DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "total group count = %d", group_count);
    DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "-----------------------------------------------------\n");
}

static void reg_type_show()
{
    DBG_PRINT(APP_INFO, NM_DBSW,"all reg tye:");

    struct reg_type *tmp_reg;
    struct list_head *pos_reg;

    list_for_each(pos_reg, &reg_list){
        tmp_reg = list_entry(pos_reg, struct reg_type, list);  //找到寄存器类型
        DBG_PRINT(APP_INFO NM_DBSW "%c ", tmp_reg->type);
    }
    printf("\n");
}

static int set_node_mask(struct group_info *tmp_group, struct cloud_node_obj node_query)
{
    unsigned int tmp_mask = 0;
    unsigned int idx = 0;

    if(tmp_group == NULL){
        DBG_LEVEL_APP(APP_ERR, NM_NAME,"set_node_mask error");
        return -1;
    }

    if(node_query.mdbs_addr > tmp_group->group_end_addr\
            || node_query.mdbs_addr < tmp_group->group_start_addr){
        DBG_LEVEL_APP(APP_ERR, NM_NAME, "error! address is failed,addr:%d start:%d end:%d",\
                      node_query.mdbs_addr, tmp_group->group_start_addr, tmp_group->group_end_addr);
        return -1;
    }

    if((node_query.mdbs_addr - tmp_group->group_start_addr) % tmp_group->reg_addr_space != 0){
        DBG_LEVEL_APP(APP_ERR, NM_NAME, "error! address is failed,addr:%d addr space:%d",\
                      node_query.mdbs_addr, tmp_group->reg_addr_space);
        return -1;
    }

    idx = (node_query.mdbs_addr - tmp_group->group_start_addr) / tmp_group->reg_addr_space;
    assert(idx < ONE_GROUP_MAX_COUNT);

    tmp_mask = tmp_group->node_mask;
    BIT_SET(idx, (unsigned char *)&tmp_group->node_mask);

    DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "oldmask:%x newmask:%x", \
                  tmp_mask, tmp_group->node_mask);

    return 0;
}

/**
 * @brief 把节点添加到相应的分组中
 * @param tmp_reg
 * @param node_query
 * @return
 */
static int add2group(struct reg_type *tmp_reg, struct cloud_node_obj node_query)
{
    struct list_head *pos;
    struct group_info *tmp_group;

    list_for_each(pos, &tmp_reg->group_list){
        tmp_group = list_entry(pos, struct group_info, list);
        if(tmp_group->group_start_addr <= node_query.mdbs_addr \
                && tmp_group->group_end_addr >= node_query.mdbs_addr\
                && tmp_group->sid == node_query.sid && tmp_group->vk == node_query.datatype){
            tmp_group->group_node_count++;
            DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "%c%d mbaddr = %d, add to group idx=%d, vk = %d",\
                          node_query.plc_regtype, node_query.plc_regnumber, node_query.mdbs_addr, tmp_group->idx, tmp_group->vk);
            set_node_mask(tmp_group, node_query);
            return 0;
        }
    }

    return -1;
}

static int set_group_start_addr(struct reg_type *tmp_reg, struct group_info *tmp_group, \
                                struct cloud_node_obj node_query)
{
    unsigned int idx;
    if(node_query.mdbs_addr > tmp_reg->reg_end_addr\
            ||node_query.mdbs_addr < tmp_reg->reg_start_addr){
        DBG_LEVEL_APP(APP_ERR, NM_NAME, "%c set_group_start_addr error! node addr:%d type start:%d type end:%d",\
                      tmp_reg->type, node_query.mdbs_addr, tmp_reg->reg_start_addr, tmp_reg->reg_end_addr);
        return -1;
    }

    if((node_query.mdbs_addr - tmp_reg->reg_start_addr) % tmp_group->reg_addr_space != 0){
        DBG_LEVEL_APP(APP_ERR, NM_NAME, "%c error! address is failed,addr:%d addr space:%d",\
                      tmp_reg->type,node_query.mdbs_addr,tmp_group->reg_addr_space);
        return -1;
    }

    idx = (node_query.mdbs_addr - tmp_reg->reg_start_addr) / (tmp_group->reg_addr_space * tmp_reg->reg_max_count);
    tmp_group->group_start_addr = tmp_reg->reg_start_addr + idx * (tmp_group->reg_addr_space * tmp_reg->reg_max_count);

    return 0;
}

static int set_group_end_addr(struct reg_type *tmp_reg,struct group_info *tmp_group, \
                              struct cloud_node_obj node_query)
{
    unsigned int tmp_addr;
    unsigned int idx;

    if(node_query.mdbs_addr>tmp_reg->reg_end_addr || node_query.mdbs_addr<tmp_reg->reg_start_addr){
        DBG_LEVEL_APP(APP_ERR, NM_NAME, "%c error! address is failed,addr:%d start:%d end:%d",\
                      tmp_reg->type, node_query.mdbs_addr, tmp_reg->reg_start_addr, tmp_reg->reg_end_addr);
        return -1;
    }

    if((node_query.mdbs_addr - tmp_reg->reg_start_addr)%tmp_group->reg_addr_space != 0){
        DBG_LEVEL_APP(APP_ERR, NM_NAME, "%c error! address is failed,addr:%d addr space:%d",\
                      tmp_reg->type, node_query.mdbs_addr, tmp_group->reg_addr_space);
        return -1;
    }

    idx = (node_query.mdbs_addr - tmp_reg->reg_start_addr) / (tmp_group->reg_addr_space * tmp_reg->reg_max_count);
    tmp_addr = tmp_reg->reg_start_addr + ((idx + 1) * (tmp_group->reg_addr_space * tmp_reg->reg_max_count)) - tmp_group->reg_addr_space;
    tmp_group->group_end_addr = MIN(tmp_addr, tmp_reg->reg_end_addr - tmp_group->reg_addr_space + 1);

    return 0;
}

int node_bit_isset(struct group_info *tmp_group, int idx)
{
    assert(tmp_group != NULL);
    assert(idx < tmp_group->parent_reg->reg_max_count);

    return BIT_ISSET(idx, (uint8 *)&tmp_group->node_mask);
}

/**
 * @brief 创建组并将节点添加
 * @param tmp_reg
 * @param node_query
 * @return
 */
static int create_group_add_node(struct reg_type *tmp_reg, struct cloud_node_obj node_query)
{
    struct group_info *tmp_group = NULL;

    if(tmp_reg == NULL){
        DBG_LEVEL_APP(APP_ERR, NM_NAME, "tmp_reg is null");
        return -1;
    }

    tmp_group = (struct group_info *)malloc(sizeof(struct group_info));
    memset(tmp_group, 0, sizeof(struct group_info));

    //设置组参数
    tmp_group->idx = tmp_reg->group_count++;
    tmp_group->sid = node_query.sid;
    tmp_group->vk = node_query.datatype;
    tmp_group->group_node_count = 1;
    tmp_group->group_limit_nc = tmp_reg->reg_max_count;
    tmp_group->node_mask = 0;
    tmp_group->data_avl = false;
    tmp_group->parent_reg = tmp_reg;

    pthread_mutex_init(&tmp_group->mutex_lock, NULL);

    //设置好地址间隔
    if(tmp_reg->set_addr_space(tmp_group) < 0){
        DBG_LEVEL_APP(APP_ERR, NM_NAME, "set_addr_space error! node addr is %d, vk = %d", node_query.mdbs_addr,tmp_group->vk);
        goto ERR;
    }

    if(set_group_start_addr(tmp_reg, tmp_group, node_query) < 0){
        DBG_LEVEL_APP(APP_ERR, NM_NAME, "set_group_start_addr error! node addr is %d", node_query.mdbs_addr);
        goto ERR;
    }
    if(set_group_end_addr(tmp_reg, tmp_group, node_query) < 0){
        DBG_LEVEL_APP(APP_ERR, NM_NAME, "set_group_end_addr error! node addr is %d",node_query.mdbs_addr);
        goto ERR;
    }

    set_node_mask(tmp_group, node_query);

    //加入链表
    list_add_tail(&tmp_group->list, &tmp_reg->group_list);

    DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "create group idx=%d, start add=%d, end addr=%d, space=%d",\
                  tmp_group->idx, tmp_group->group_start_addr, tmp_group->group_end_addr, tmp_group->reg_addr_space);
    DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "%c%d mbaddr = %d, add to group idx=%d, vk = %d",\
                  node_query.plc_regtype, node_query.plc_regnumber, node_query.mdbs_addr, tmp_group->idx, tmp_group->vk);

    group_count++;
    return 0;

ERR:
    free(tmp_group);
    return -1;
}

static int add_node2reg_list(struct reg_type *tmp_reg, struct cloud_node_obj node_query)
{
    assert(tmp_reg != NULL);

    //判断是否有该节点的组,若有加入该节点，没有则创建并加入
    if(add2group(tmp_reg, node_query) < 0){
        if(create_group_add_node(tmp_reg,node_query)<0){
            DBG_LEVEL_APP(APP_ERR, NM_NAME, "create_group_add_node error! node addr is %d", node_query.mdbs_addr);
            return -1;
        }
    }
    return 0;
}

/**
 * @brief 节点分组处理：找到节点所在的类型链表，然后放到相应的组中
 * @param node_query
 * @return
 */
static int node_group_handle(struct cloud_node_obj node_query)
{
    struct list_head *pos;
    int ret;

    DBG_PRINT(APP_INFO NM_DBSW "\n");

    struct reg_type *tmp_reg;
    list_for_each(pos, &reg_list){
        tmp_reg = list_entry(pos, struct reg_type, list);
        if(tmp_reg->type == node_query.plc_regtype){
            DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "-----------------------------------------------------");
            DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "will merge %c%d mbaddr = %d", \
                          node_query.plc_regtype, node_query.plc_regnumber, node_query.mdbs_addr);
            if(add_node2reg_list(tmp_reg, node_query) < 0){
                ret = 0;
            }else{
                ret = 1;
            }
            break;
        }
    }

    if(0 == ret){
        DBG_LEVEL_APP(APP_ERR, NM_NAME, "merge error!", node_query.mdbs_addr);
        return -1;
    }else{
        DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "merge success");
        return 0;
    }
}


static int node_group()
{
    int avl_num = 0;
    int i = 0;

    avl_num = CloudNodeOpt_GetAvlNum();

    if(avl_num <=0){
        DBG_LEVEL_STD(APP_ERR, NM_DBSW, NM_NAME, "error !! node count is zero! available_num = %d",avl_num);
        return -1;
    }

    while(i < avl_num){
        if(node_group_handle(CloudNodeOpt_GetNode(i))<0){
            DBG_LEVEL_APP(APP_ERR, NM_NAME, "error !! node merge is failed! idx:%d",i);
            return -1;
        }
        i++;
    }
    DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "-----------------------------------------------------");

    return 0;
}


/**
 * @brief 增加新的寄存器类型
 * @param new_reg
 * @return
 */
int add_reg_type(struct reg_type new_reg)
{
    if(new_reg.reg_max_count > ONE_GROUP_MAX_COUNT){
        DBG_LEVEL_STD(APP_ERR, NM_DBSW, NM_NAME, "limit max is 32");
        return -1;
    }

    struct reg_type *tmp_reg = (struct reg_type *)malloc(sizeof(struct reg_type));
    if(NULL == tmp_reg){
        DBG_LEVEL_STD(APP_ERR, NM_DBSW, NM_NAME, "error !! malloc is failed!");
        return -1;
    }
    memset(tmp_reg, 0, sizeof(struct reg_type));

    *tmp_reg = new_reg;
    tmp_reg->group_count = 0;
    INIT_LIST_HEAD(&tmp_reg->group_list);  //初始化组链表头

    list_add_tail(&tmp_reg->list, &reg_list);

    reg_type_count++;

    return 0;
}

/**
 * @brief 把各组查询加入poll层
 * @return 0成功，-1失败
 */
int add_group2poll()
{
    struct list_head *pos_reg, *pos_group;
    struct reg_type *tmp_reg;
    struct group_info *tmp_group;

    list_for_each(pos_reg, &reg_list){
        tmp_reg = list_entry(pos_reg, struct reg_type, list);  //找到寄存器类型
        list_for_each(pos_group, &tmp_reg->group_list){
            tmp_group = list_entry(pos_group, struct group_info, list);
            if(tmp_reg->register2poll(tmp_group) != 0){
                DBG_LEVEL_APP(APP_ERR, NM_NAME, "register error");
                continue;
            }
        }
    }
#if 0  //测试定时类型
    struct list_head list_tmp;
    INIT_LIST_HEAD(&list_tmp);

    list_for_each(pos_reg, &reg_list){
        tmp_reg = list_entry(pos_reg, struct reg_type, list);  //找到寄存器类型
        list_for_each(pos_group, &tmp_reg->group_list){
            tmp_group = list_entry(pos_group, struct group_info, list);
            struct plc_poll_obj *poll_obj = pool_get_node();
            poll_obj->type = TIME_T;
            poll_obj->group = group_info;
            poll_obj->read_session = mb_read_session;
            list_add(poll_obj->list, &list_tmp);
        }
    }
    add_poll_list(list_tmp);
#endif

    return 0;
}

/**
 * @brief 根据当前的PLC协议进行不同的初始化
 * @return
 */
int node_merge_init()
{
    INIT_LIST_HEAD(&reg_list);

    int ret = node_merge_init_modbus();
    DBG_LEVEL_STD(APP_INFO, NM_DBSW, NM_NAME, "reg type total num = %d", reg_type_count);
    reg_type_show();

    //将节点进行分组
    ret = node_group();
    if(ret != 0 ){
        DBG_LEVEL_STD(APP_ERR, NM_DBSW, NM_NAME, "error !! node_merge failed! ");
    }

    group_limit_begin_end();  //重新计算组首位地址
    group_info_show();  //打印组信息
    add_group2poll();  //把各组查询加入poll层

    return 0;
}

