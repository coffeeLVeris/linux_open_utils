/***********************************************************************************
Copy right:	2016-2026 USR IOT Tech.
Author:			焦岳
Version:		V1.0
Date:			2019-01
Description:	打印组件封装
***********************************************************************************/

#ifndef __DEBUG_PRINT_H
#define __DEBUG_PRINT_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>

#define DEFAULT_DBGLEVEL  5
#define MAX_BUF_LEN 4096  //打印内容不能超过这个，否则会出段错误

//总打印级别控制
#define	APP_EMERG	"<0>"	/* system is unusable			*/
#define	APP_ALERT	"<1>"	/* action must be taken immediately	*/
#define	APP_CRIT	"<2>"	/* critical conditions			*/
#define	APP_ERR	    "<3>"	/* error conditions			*/
#define	APP_WARNING	"<4>"	/* warning conditions			*/
#define	APP_NOTICE	"<5>"	/* normal but significant condition	*/
#define	APP_INFO	"<6>"	/* informational			*/
#define	APP_DEBUG	"<7>"	/* debug-level messages			*/

#define	MODULE_DBG_ON 	"(1)"
#define	MODULE_DBG_OFF 	"(0)"


int DebugPrint(const char *format, ...);
void set_dbg_limit(const char *buf);
long get_sys_runtime(int type);

/************************使用下面宏进行打印*****************************/
//可添加打印级别控制
#define DBG_PRINTF DebugPrint

//带文件和行号的普通打印(强制打印)
#define DBG_LINE(format,...) printf("[%s][%d]:" format "\r\n", __FILE__, __LINE__, ##__VA_ARGS__)

/**
 * 定制格式打印--带调试级别(打印受APP和MODULE两个开关控制)
 * @param  APP_LEVEL    [APP打印级别]
 * @param  MODULE_LEVEL [模块打印级别]
 * @param  MODULE_NAME  [模块名称]
 * @param  CONTENT      [打印内容]
 */
#define DBG_LEVEL_STD(APP_LEVEL, MODULE_LEVEL, MODULE_NAME, CONTENT, ...)\
    DBG_PRINTF(APP_LEVEL MODULE_LEVEL "[%05ld.%06ld][%s][%s][%d]:" CONTENT "\r\n", \
        get_sys_runtime(1), get_sys_runtime(2), MODULE_NAME, __FILE__, __LINE__, ##__VA_ARGS__)

/**
 * 定制格式打印--带调试级别(打印只受APP级别控制)
 * @param  APP_LEVEL    [APP打印级别]
 * @param  MODULE_NAME  [模块名称]
 * @param  CONTENT      [打印内容]
 */
#define DBG_LEVEL_APP(APP_LEVEL, MODULE_NAME, CONTENT, ...)\
    DBG_PRINTF(APP_LEVEL MODULE_DBG_ON "[%05ld.%06ld][%s][%s][%d]:" CONTENT "\r\n", \
        get_sys_runtime(1), get_sys_runtime(2), MODULE_NAME, __FILE__, __LINE__, ##__VA_ARGS__)

/**
 * 定制格式打印--不带调试级别（一定会打印）
 * @param  MODULE_NAME  [模块名称]
 * @param  CONTENT      [打印内容]
 */
#define DBG_STD(MODULE_NAME, CONTENT, ...)\
    printf("[%05ld.%06ld][%s][%s][%d]:" CONTENT "\r\n", \
        get_sys_runtime(1), get_sys_runtime(2), MODULE_NAME, __FILE__, __LINE__, ##__VA_ARGS__)


/*****************这块可统一放置各个模块的控制开关*******************/
//这块内容软件规模小时候放到这可以，软件规模比较大时候需要分散到个模块内部。
//一般需要模块打印开关和模块名字两个定义。

//主动上报模块
//#define AU_DBSW MODULE_DBG_ON
//#define AU_NAME "active_upload"

/**************************************************************/

#endif
