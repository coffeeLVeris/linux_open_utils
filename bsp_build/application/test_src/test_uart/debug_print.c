/***********************************************************************************
Copy right:	2016-2026 USR IOT Tech.
Author:			焦岳
Version:		V1.0
Date:			2019-01
Description:	打印组件封装
***********************************************************************************/

#include "debug_print.h"

static int level_limit = DEFAULT_DBGLEVEL;

void set_dbg_limit(const char *buf)
{
    if((buf[0] == '<') && (buf[2] == '>')){
        int dbglevel = buf[1] - '0';
        if(dbglevel >= 0 && dbglevel <= 7){
            level_limit = dbglevel;
        }
    }
}

int DebugPrint(const char *format, ...)
{
    char str_tmp[MAX_BUF_LEN];
    char *tmp;
    va_list arg;
    int len;
    int dbglevel = DEFAULT_DBGLEVEL;
    
    va_start (arg, format);
    len = vsprintf(str_tmp, format, arg);
    va_end (arg);
    str_tmp[len] = '\0';

    tmp = str_tmp;
    
    //printf("<4><1><5>");
    // 根据打印级别决定是否打印
    if((str_tmp[0] == '<') && (str_tmp[2] == '>')){
        dbglevel = str_tmp[1] - '0';
        if (dbglevel >= 0 && dbglevel <= 9){
            tmp = tmp + 3;
        }else{
            dbglevel = DEFAULT_DBGLEVEL;
        }
    }

    // 根据模块判断是否打印
    if((str_tmp[3] == '(') && (str_tmp[5] == ')')){
        int on = str_tmp[4] - '0';
        if(on == 1){
            tmp = tmp + 3;
        }else{
            return -1;
        }
    }

    if (dbglevel > level_limit){
        return -1;
    }

    printf("%s", tmp);

    return 0;
    
}

long get_sys_runtime(int type)
{
    struct timespec times = {0, 0};
    long time;

    clock_gettime(CLOCK_MONOTONIC, &times);

    if (1 == type){
        time = times.tv_sec;
    }else{
        time = times.tv_nsec / 1000000;
    }

    // printf("time = %ld\n", time);
    return time;
}
