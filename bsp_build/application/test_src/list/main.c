#include <stdio.h>
#include <string.h>
#include "list.h"

struct obj
{
    char name[64];
    int age;
    struct list_head list;
};

static struct list_head handler_head;
static struct list_head list2;
static struct list_head list3;

static int test_list()
{
    struct obj *stu5 = (struct obj *)malloc(sizeof(struct obj));
    strcpy(stu5->name, "abc");
    stu5->age = 124;

    struct obj *stu6 = (struct obj *)malloc(sizeof(struct obj));;
    strcpy(stu6->name, "fsdf");
    stu6->age = 456;    

    list_add_tail(&stu5->list, &list3);
    list_add_tail(&stu6->list, &list3);    
}


int main(int argc, char const *argv[])
{
    struct obj *obj_tmp;
    INIT_LIST_HEAD(&handler_head);
    INIT_LIST_HEAD(&list2);
    INIT_LIST_HEAD(&list3);
    struct list_head *pos;

#if 0
    struct obj stu1;
    strcpy(stu1.name, "zhangsan");
    stu1.age = 10;

    struct obj stu2;
    strcpy(stu2.name, "lisi");
    stu2.age = 20;

    struct obj stu3;
    strcpy(stu3.name, "wangwu");
    stu3.age = 30;

    struct obj stu4;
    strcpy(stu4.name, "zhangliu");
    stu4.age = 40;

    list_add_tail(&stu1.list, &handler_head);
    list_add_tail(&stu2.list, &handler_head);

    list_add(&stu3.list, &list2);
    list_add(&stu4.list, &list2);

    list_splice(&list2, &handler_head);

    list_for_each(pos, &handler_head)
    {
        obj_tmp = list_entry(pos, struct obj, list);
        printf("support handler name:%s\n", obj_tmp->name);
        printf("support handler name:%d\n", obj_tmp->age);
    }
#endif

    test_list();
    list_for_each(pos, &list3)
    {
        obj_tmp = list_entry(pos, struct obj, list);
        printf("support handler name:%s\n", obj_tmp->name);
        printf("support handler name:%d\n", obj_tmp->age);
    }

    // printf("\n");
    // list_for_each_prev(pos, &handler_head)
    // {
    //     obj_tmp = list_entry(pos, struct obj, list);
    //     printf("support handler name:%s\n", obj_tmp->name);
    //     printf("support handler name:%d\n", obj_tmp->age);
    // }

    return 0;
}
