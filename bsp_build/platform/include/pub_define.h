/**
 ** @author:   焦岳
 ** @date:	   2019.1.3
 ** @brief:    类型重定义
 */
#ifndef __PUB_DEFINE_H
#define __PUB_DEFINE_H

#include "public.h"

#define bool int
#define true 1
#define false 0

#define KB 1024

#if OS_TYPE == OS_LINUX
#include <stdint.h>

#define uint8  uint8_t
#define uint16 uint16_t
#define uint32 uint32_t

#define int8  int8_t
#define int16 int16_t
#define int32 int32_t

#elif OS_TYPE == OS_RT



#endif  //OS_LINUX

#endif //__PUB_DEFINE_H
