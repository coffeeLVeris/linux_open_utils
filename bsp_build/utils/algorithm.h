/***********************************************************************************
Copy right:     2016-2026 USR IOT Tech.
Author:         焦岳
Version:        V1.0
Date:           2019-05
Description:    通用算法模块
***********************************************************************************/

#ifndef __ALGORITHM_H
#define __ALGORITHM_H

#include "pub_define.h"

int oct2dec(char *s);
uint8 calc_chesum(const uint8 *data, int start, int end);
uint8 calc_bcc(const uint8 *data, int start, int end);


#define MAX(a,b) ((a)>(b)?(a):(b))
#define MIN(a,b) ((a)>(b)?(b):(a))

#endif
