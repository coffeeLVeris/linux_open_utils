
#ifndef __FILE_OPT_H
#define __FILE_OPT_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

long FileOpt_GetSize(const char *path);
size_t FileOpt_ReadAll(const char *path, char *ret_buf);

#endif
