#include "ltpro.h"
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[] )
{
    if(argc != 2){
        printf("usage:%s <style>\n", argv[0]);
        puts("1:数字型");
        puts("2:#型");
        puts("3:图案");
        return -1;
    }
    
    int style = 1;
    int tmp = atoi(argv[1]);
    switch (tmp){
        case 1:
        style = PROGRESS_NUM_STYLE;
        break;
        case 2:
        style = PROGRESS_CHR_STYLE;
        break;
        case 3:
        style = PROGRESS_BGC_STYLE;
        break;     
        default:
        break;
    }

    progress_t bar;
    progress_init(&bar, "进度：", 50, style);

    int i;
    for ( i = 0; i <= 50; i++ ) {
        progress_show(&bar, i/50.0f);
        usleep(50000);
    }
    printf("\n+-Done\n");

    progress_destroy(&bar);

    return 0;
}
