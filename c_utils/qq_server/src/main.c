#include "database.h"
#include "server.h"
#include "msg_handler.h"

int main(int argc, char const *argv[])
{
    int ret;
    pthread_t server_tid, msg_tid;

    //处理输入参数
    if(argc != 2){
        printf("usage: %s <port>\n", argv[0]);
        return -1;
    }

    server_port = atoi(argv[1]);

    //初始化数据库
    ret = init_database();
    if(ret < 0){
        err_log("init_database err");
        return -1;
    }

    //初始化服务器
    ret = init_server(&server_tid);
    if(ret < 0){
        err_log("init_server err");
        return -1;
    }

    //初始化消息处理机制
    ret = init_msg_handler(&msg_tid);
    if(ret < 0){
        err_log("init_msg_handler err");
        return -1;
    }

    //等待线程
    pthread_join(server_tid, NULL);
    pthread_join(msg_tid, NULL);

	return 0;
}
