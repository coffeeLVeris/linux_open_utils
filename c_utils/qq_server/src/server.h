/**
 ** @author: 浓咖啡
 ** @date:  2018.5.11
 ** @brief: 服务器初始化
 */

#ifndef __SERVER__H__
#define __SERVER__H__

#include "data_global.h"
#include <sys/epoll.h>

int init_server(pthread_t *);

#endif
