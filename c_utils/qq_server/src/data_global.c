/**
 ** @author: 浓咖啡
 ** @date:  2017.7.4
 ** @brief: 全局结构定义
 */

#include "data_global.h"

//线程通信相关
pthread_cond_t cond_transfer;

pthread_mutex_t mutex_transfer;
pthread_mutex_t mutex_linklist;

//全局控制变量
uint16_t server_port = 8888;  //服务器绑定端口
