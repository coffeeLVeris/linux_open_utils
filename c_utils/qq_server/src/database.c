/**
 ** @author: 浓咖啡
 ** @date:  2018.5.11
 ** @brief: 数据库相关操作
 */

#include "database.h"

#define DATABASE_NAME "qq_server.db"

static sqlite3 *db = NULL;

#define CREATE_USER_INFO "\
create table if not exists user_info(\
        qq integer primary key,\
        net_addr blob,\
        nickname text,\
        sex integer)\
"

/**
 * @brief 初始化数据库表
 * @return 0:成功 -1失败
 */
int init_database(void)
{
    int ret;

    ret = sqlite3_open(DATABASE_NAME, &db);
    if (ret != SQLITE_OK){
        err_log("can't open database:%s!\n", sqlite3_errmsg(db));
        return -1;
    }
    LOG("open database ok");

    //首先创建用户信息表
    ret = sqlite3_exec(db, CREATE_USER_INFO, 0, 0, NULL);
    if (ret != SQLITE_OK){
        err_log("create table err = %s\n", sqlite3_errmsg(db));
        return -1;
    }else{
        return 0;
    }
}




