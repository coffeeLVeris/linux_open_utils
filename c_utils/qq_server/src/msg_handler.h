/**
 ** @author: 浓咖啡
 ** @date:  2018.5.29
 ** @brief: 消息处理分发模块
 ** 此线程主要用于从消息缓冲区中分解消息并且分发出去
 */

#ifndef __MSG_HANDLER__H__
#define __MSG_HANDLER__H__

#include "data_global.h"
#include "list.h"

typedef enum{
    LOGIN
}MSG_TYPE;

/**
 * @brief 消息处理核心结构体
 * name:模块名称
 * table_id:指向支持的消息类型数组
 * contain:模块提供接口表示是否能支持此类型消息，这样模块可以支持多类消息
 * handler:消息处理函数，这个处理不能阻塞，而且要快，消息要进行复制
 */
struct handle_obj{
    char name[64];
    MSG_TYPE *table_id;
    int (*contain)(MSG_TYPE type);
    bool (*handler)(void *data);
    struct list_head list;
};

typedef int MSG_LEN_TYPE;

//这里尝试使用了可变长数组，但是这样效率可能会降低
//还有一种办法是定义最大消息长度，但是那样不通用
struct msg{
    MSG_LEN_TYPE data_len;  //消息总长
    char data[0];  //消息正文
};

int init_msg_handler(pthread_t *tid);
void register_handle(struct handle_obj *obj);
void unregister_handle(struct handle_obj *obj);

#endif
