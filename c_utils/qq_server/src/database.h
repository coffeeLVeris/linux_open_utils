/**
 ** @author: 浓咖啡
 ** @date:  2018.5.11
 ** @brief: 数据库相关操作
 */

#ifndef __DATABASE__H__
#define __DATABASE__H__

#include "data_global.h"
#include "sqlite3.h"

int init_database(void);

#endif
