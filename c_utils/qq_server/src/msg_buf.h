/**
 ** @author: 浓咖啡
 ** @date:  2018.5.22
 ** @brief: 实现消息的buf缓冲区，类似与socket缓冲区功能
 */

#ifndef __MSG_BUF__H__
#define __MSG_BUF__H__

#include "data_global.h"

int msg_read(void *buf, size_t count);
int msg_write(const void *buf, size_t count);
int msg_len();

#endif
