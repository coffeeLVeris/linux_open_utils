/**
 ** @author: 浓咖啡
 ** @date:  2018.5.30
 ** @brief: 登录消息处理模块
 */
#include "login_handle.h"

//只支持登录消息处理
static MSG_TYPE table_type[] = {LOGIN};
static struct handle_obj login_handle;

static int login_msg_handler(void *data);
static bool suppurt_msg(MSG_TYPE type);

int init_login_handle()
{
    strcpy(login_handle.name, "login");
    login_handle.table_id = table_type;
    login_handle.contain = suppurt_msg;
    login_handle.handler = login_msg_handler;

    register_handle(&login_handle);

    return 0;
}


static int login_msg_handler(void *data)
{
    struct msg_login msg = *(struct msg_login *)data;
    LOG("usr_name = %s", msg.usr_name);
    LOG("passwd = %s", msg.passwd);

    //业务逻辑
}

static bool suppurt_msg(MSG_TYPE type)
{
    int i;
    for(i=0; i<sizeof(table_type)/sizeof(table_type[0]); i++){
        if(type == table_type[i]){
            return true;
        }
    }
    return false;
}
