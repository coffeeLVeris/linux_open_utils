/**
 ** @author: 浓咖啡
 ** @date:  2018.5.30
 ** @brief: 登录消息处理模块
 */

#ifndef __LOGIN_HAND__H__
#define __LOGIN_HAND__H__

#include "msg_handler.h"

struct msg_login{
    MSG_LEN_TYPE msg_len;
    MSG_TYPE type;
    char usr_name[64]; //用户名
    char passwd[64];  //密码
};

int init_login_handle();


#endif
