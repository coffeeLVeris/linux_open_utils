/**
 ** @author: 浓咖啡
 ** @date:  2017.7.4
 ** @brief: 全局结构定义
 */

#ifndef __DATA_GLOBAL__H__
#define __DATA_GLOBAL__H__

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <pthread.h>
#include <sys/select.h>
#include <pthread.h>

#define ANALOG  //是否利用模拟数据产生

//宏调试
#define __LOG__

#ifdef __LOG__
#define LOG(format,...) printf(__FILE__ ":%d: " format "\n", __LINE__, ##__VA_ARGS__)
#else
#define LOG(format,...)
#endif

#define SEND_INTERVAL 1  //发送间隔,单位秒

#define err_log(format,...) printf(__FILE__ ":%d: " format "\n", __LINE__, ##__VA_ARGS__)

#define true 1
#define false 0
typedef int bool;

extern pthread_cond_t cond_transfer;
extern pthread_mutex_t mutex_transfer;
extern pthread_mutex_t mutex_linklist;

extern uint16_t server_port;

#endif
