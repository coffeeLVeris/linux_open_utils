#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sqlite3.h>

int main(int argc, const char *argv[])
{
	// create table tabel8(id int primary key,ATTR BLOB not null);
	char sql[64] = "insert into tabel8 values (?, ?)";

	sqlite3 *db;
	int ret;

	sqlite3_open("my.db", &db);

	const char *error;
	//调用step后如果出错，会被置为空值
	sqlite3_stmt *stmt;

	struct A
	{
		int x;
		char  y;
	} a = {1, 'a'};

	char *errmsg;
	if(sqlite3_exec(db, "delete from tabel8",  NULL,  NULL, &errmsg) !=  SQLITE_OK)
	{
		 printf("error:%s\n",  errmsg);
		 exit(-1);
	}

	sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, &error);

	sqlite3_bind_int(stmt, 1, 1);
	sqlite3_bind_blob(stmt, 2, &a, sizeof(a), NULL);

	ret = sqlite3_step(stmt);
	if(ret != SQLITE_DONE){
		printf("%s\n",sqlite3_errmsg(db)); 
	}

	sqlite3_reset(stmt);
	a.x = 2;
	a.y = 'b';

	sqlite3_bind_int(stmt, 1, 2);
	sqlite3_bind_blob(stmt, 2, &a, sizeof(a), NULL);
	ret = sqlite3_step(stmt);
	if(ret != SQLITE_DONE){
		printf("%s\n",sqlite3_errmsg(db)); 
	}

	sqlite3_finalize(stmt); 

	char buf[64] = "select * from tabel8";
	ret = sqlite3_prepare(db, buf, strlen(buf), &stmt, &error);

	while(1){
		ret = sqlite3_step(stmt);
		if(ret != SQLITE_ROW){
			// printf("%s\n",sqlite3_errmsg(db)); 
			break;
		}

		struct A *value = (struct A *)sqlite3_column_blob(stmt, 1);
		printf("x = %d y =%c\n", value->x, value->y);
	}

	sqlite3_close(db);

	return 0;
}

