#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sqlite3.h>

static sqlite3 *db = NULL;

#define DATABASE_NAME "qq_server.db"

#define CREATE_TABLE \
    "create table if not exists user_info(\
        qq integer primary key,\
        net_addr blob,\
        nickname text,\
        sex integer)"

/**
 * @brief 初始化数据库表
 * @return 0:成功 -1失败
 */
int init_database(void)
{
    char sql[1024];
    int ret;

    ret = sqlite3_open(DATABASE_NAME, &db);
    if (ret != SQLITE_OK){
        printf("can't open database:%s!\n", sqlite3_errmsg(db));
        return -1;
    }else{
        printf("open database ok\n");
        strcpy(sql, CREATE_TABLE);
        ret = sqlite3_exec(db, CREATE_TABLE, 0, 0, NULL);
        if (ret != SQLITE_OK){
            printf("err = %s\n", sqlite3_errmsg(db));
        }
    }
}

int main(int argc, char const *argv[])
{
	init_database();
	return 0;
}
