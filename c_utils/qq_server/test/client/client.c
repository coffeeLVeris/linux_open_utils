#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define N 64

struct msg_login{
    int msg_len;
    int type;
    char usr_name[64]; //用户名
    char passwd[64];  //密码
};

int main(int argc, char *argv[])
{
    // 0定义变量
    int sockfd;
    char buf[N];
    int addrlen = sizeof(struct sockaddr);
    struct sockaddr_in serveraddr;

    if(argc != 3){
        printf("usage: %s <ip> <port>\n", argv[0]);
        return -1;
    }

    // 1创建一个套接字--socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0){
        perror("socket err");
        exit(-1);
    }

    // 2指定服务器地址--sockaddr_in
    bzero(&serveraddr, addrlen);
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = inet_addr(argv[1]);
    serveraddr.sin_port = htons(atoi(argv[2]));

    // 3连接服务器--connect
    if(connect(sockfd, (struct sockaddr *)&serveraddr, addrlen) < 0){
        perror("connect err");
        exit(-1);
    }

    struct msg_login msg;
    msg.msg_len = sizeof(msg);
    msg.type = 0;
    strcpy(msg.usr_name, "1075518049");
    strcpy(msg.passwd, "xc456789123");

    send(sockfd, &msg, sizeof(msg), 0);

    // 4收发数据--recv/send
    // while (1) {
    //     //再发剩下的
    //     send(sockfd, buf, len, 0);
    // }

    // 5关闭连接--close
    close(sockfd);
}

