#ifndef _CONFIG_STREAM_H_
#define _CONFIG_STREAM_H_

/**
    *file_absolute_path 文件绝对路径(in)
    *key 要查询的键(in)
    **value 输出的键值(out)
    *len 键值长度(in)
*/
int intput_config_value(char *file_absolute_path,char *key,char **value,int *len);

/**
*file_absolute_path 文件绝对路径(in)
*key 要增加的键(in)
*value 要增加的键值(in)
*/
int output_config_key_value(char *file_absolute_path, char *key, char *value);
#endif