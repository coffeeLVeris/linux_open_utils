
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "config_stream.h"

/* 文件绝对路径 */
#define FILE_ABS_PATH "abc.txt"

void main() {
    char *key = "key4";
    char *value = "hello world";
    //写
    output_config_key_value(FILE_ABS_PATH,key,value);

    // 读
    int len;
	key = "key4";
    char *values;
    int ret2 = intput_config_value(FILE_ABS_PATH,key,&values,&len);
    printf("value=%s\n",values);
    free(values);
}