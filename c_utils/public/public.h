/**
 ** @author:   浓咖啡
 ** @date:	   2019.1.3
 ** @brief:    公共定义
 */
 
#ifndef __PUBLIC_H
#define __PUBLIC_H

/*************公共宏定义******************/
#define RUN_ARM

/*************公共头文件定义******************/
#include <stdio.h>
#include <string.h>

#endif  //__PUBLIC_H
