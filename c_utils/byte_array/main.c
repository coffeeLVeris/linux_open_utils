#include "byte_array.h"

#define CHECK_SUM

int main(int argc, char const *argv[])
{
	int ret, i;

#ifdef STR2HEX
	unsigned char dst[10];
	char src[10] = "12340a";

	ret = ba_str2hex(dst, src, strlen(src));
	if(ret < 0){
		printf("err ba_str2hex\n");
		return -1;
	}

	for(i=0; i<sizeof(dst); i++){
		printf("0x%02x ", dst[i]);
	}
	printf("\n");
#elif defined GET_BIT
	unsigned char dst[10] = {0};
	unsigned char src[10] = {0x35, 0x84, 0x12, 0x55};

	get_bit(dst, src, 10, 13);

	//结果应为0xa1 0x04
	for(i=0; i<sizeof(dst); i++){
		printf("0x%02x ", dst[i]);
	}
	printf("\n");

	int a = 0x10001020;
	printf("bit 4 = %d\n", BIT_ISSET(4, (char *)&a));
	printf("bit 28 = %d\n", BIT_ISSET(28, (char *)&a));
#elif defined CHECK_SUM
	unsigned char data[20] = {0x35, 0x84, 0x12, 0x55, 0x35, 0x84, 0x12, 0x55, 0x35, 0x84, 0x12, 0x55};
	int sum = calc_chesum(data, 0, 5);
	printf("sum = 0x%x\n", sum);
#endif

	return 0;
}