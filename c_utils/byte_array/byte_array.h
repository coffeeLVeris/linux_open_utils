/**
 ** @author:   浓咖啡
 ** @date:     2019.1.23
 ** @brief:    字节流处理模块
 */

#ifndef __BYTE_ARRAY_H
#define __BYTE_ARRAY_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int is_A2F(char ch);
int ba_str2hex(unsigned char *dst, const char *src, int len);

int BIT_ISSET(int n, unsigned const char *buf);
void BIT_SET(int n, unsigned char *buf);
void BIT_ZERO(int n, unsigned char *buf);
void get_bit(unsigned char *dst, const unsigned char *src, int start, int num);
unsigned char calc_chesum(const unsigned char *data, int start, int end);

#endif

