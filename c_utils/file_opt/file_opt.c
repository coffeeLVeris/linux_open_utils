#include "file_opt.h"

//FILE *fp不能设计成全局打开一次，每一个操作都可能针对不同的打开方式
//这里只做封装，不做通用性

long FileOpt_GetSize(const char *path)
{
    FILE *fp;
    long filesize = -1;
    fp = fopen(path, "r");
    if(fp == NULL)
    {
        return filesize;
    }
    fseek(fp, 0L, SEEK_END);
    filesize = ftell(fp);
    fclose(fp);
    return filesize;
}

/**
 * @brief 读取所有文件内容，这个方法要提供缓冲区，
 * 但是缓冲区大小需要先调用上个接口自己确定，这里并不检查是否越界
 * @param path
 * @param ret_buf
 * @return 实际读到的字节数目
 */
size_t FileOpt_ReadAll(const char *path, char *ret_buf)
{
    FILE *fp;
    size_t nbytes = 0;
    char buf[256];

    if((fp = fopen(path, "r")) == NULL)
    {
        return nbytes;
    }

    while(1)
    {
        nbytes = fread(buf, 1, 256, fp);
        if(nbytes > 0)
        {
            strncpy(ret_buf, buf, nbytes); //这里只针对文本
            ret_buf += nbytes;
        }
        else
        {
            break;
        }
    }

    fclose(fp);
    return nbytes;
}

