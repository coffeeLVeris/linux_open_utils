#include "cloud_opt_handler.h"
#include <assert.h>

static struct list_head handler_head;

/**
 * @brief 注册处理模块
 * @param obj 处理模块结构体
 */
void CloudOpt_RegisterHandle(struct handle_obj *obj)
{
    list_add(&obj->list, &handler_head);
}

/**
 * @brief 显示所有的注册模块
 */
void CloudOpt_Show(void)
{
    struct list_head *pos;
    struct handle_obj *obj_tmp;

    printf("-------------------------------\n");
    list_for_each(pos, &handler_head)
    {
        obj_tmp = list_entry(pos, struct handle_obj, list);
        printf("support handler name:%s\n", obj_tmp->name);
    }
    printf("-------------------------------\n");
}

struct handle_obj *CloudOpt_GetHandle(const char *name)
{
    struct list_head *pos;
    struct handle_obj *obj_tmp;

    list_for_each(pos, &handler_head)
    {
        obj_tmp = list_entry(pos, struct handle_obj, list);
        if(strcmp(obj_tmp->name, name) == 0)
        {
            return obj_tmp;
        }
        printf("support handler name:%s\n", obj_tmp->name);
    }

    return NULL;
}

//声明一下
void CloudOptCurl_init(void);

void CloudOpt_init(void)
{
    INIT_LIST_HEAD(&handler_head);
    CloudOptCurl_init();
    //CloudOptRaw_init();
}
