TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    http-post.c \
    cJSON.c \
    cloud_opt_handler.c \
    cloud_opt_curlhdl.c

HEADERS += \
    list.h \
    cJSON.h \
    cloud_opt_handler.h \
    md5.h

