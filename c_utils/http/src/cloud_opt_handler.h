#ifndef __CLOUD_OPT_HANDLER_H
#define __CLOUD_OPT_HANDLER_H

#include <stdio.h>
#include <string.h>
#include "list.h"

/**
 * @brief 云端请求管理结构体
 * name:模块名称 现在可选"raw"和"curl"两种
 */
struct handle_obj
{
    char name[64];
    int (*init)(const char *url);  //传递需要的参数，可扩展
    void (*release)(void);  //做一些必要的清除工作
    //执行post请求，并把结果存储到文件中
    int (*post2file)(const char *post_str, const char *file_name);
     //执行post请求，并把结果存储到字符串中，注意ret_str大小一定要大于返回结果长度，这里不做检查
    int (*post2str)(const char *post_str, char *ret_str);
    struct list_head list;
};

void CloudOpt_init(void);
void CloudOpt_RegisterHandle(struct handle_obj *obj);
void CloudOpt_Show(void);
struct handle_obj *CloudOpt_GetHandle(const char *name);

void CloudOpt_CalcMd5(unsigned char *dst, unsigned char *src, int len);

#endif


