#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	int i;
	char buf[2];

	for(i=0; i<5; i++){
		buf[i] = 'a' + i;
		printf("buf[%d] = %c\n", i, buf[i]);
	}

	char *x=malloc(10);
	x[10]='a';
	printf("x[10] = %c\n", x[10]);

	return 0;
}