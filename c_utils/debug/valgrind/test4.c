#include <stdio.h>
#include <stdlib.h>

int *func()
{
	int *p = malloc(sizeof(1000));

	return p;
}

void func_free(int *p)   
{  
	free(p);  
}  

int main(int argc, char const *argv[])
{
	int i;
	int *vec[10000];

	for(i=0; i<10000; i++)  
	{  
		vec[i]=func();  
	}


	for(i=0;i<10000;i++)  
	{  
		func_free(vec[i]);  
	}

	return 0;
}