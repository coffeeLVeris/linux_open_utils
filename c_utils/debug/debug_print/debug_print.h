/**
 ** @author:   浓咖啡
 ** @date:	   2019.1.3
 ** @brief:    打印级别控制组件
 */
 
#ifndef __DEBUG_PRINT_H
#define __DEBUG_PRINT_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>

#define true 1
#define false 0

#define KB 1024

#define DEFAULT_DBG_LIMIT  6    //APP_INFO(包括)以前会打印到终端
#define DEFAULT_LOG_LIMIT  4    //APP_WARNING(包括)之前的会同时输出到文件
#define DEFAULT_LOG_CUT_SIZE    1024*KB
#define DEFAULT_LOG_MAX_NUM    20

#define MAX_BUF_LEN 4096  //打印内容不能超过这个，否则会出段错误

struct dbg_parame{
    int dbg_limit;  //打印级别控制，小于此级别会输出到终端
    int log_limit;  //日志级别控制，小于此级别会同时输出到日志
    int log_cut_size; //日志文件分割大小，到了这个大小会自动保存为新文件
    char log_file[256]; //日志文件路径和名称
    int start_daemon; //是否启动日志守护脚本 true:自动启动，需要指定下面参数 false:不自动启动，由系统启动
    int max_log_num; //最大存储多少日志文件
    char log_path[256]; //日志文件缓存路径
    char shell_file[256]; //守护脚本路径和名称
};

//总打印级别控制
#define	APP_EMERG	"<0>"	/* system is unusable			*/
#define	APP_ALERT	"<1>"	/* action must be taken immediately	*/
#define	APP_CRIT	"<2>"	/* critical conditions			*/
#define	APP_ERR	    "<3>"	/* error conditions			*/
#define	APP_WARNING	"<4>"	/* warning conditions			*/
#define	APP_NOTICE	"<5>"	/* normal but significant condition	*/
#define	APP_INFO	"<6>"	/* informational			*/
#define	APP_DEBUG	"<7>"	/* debug-level messages			*/

#define	MODULE_DBG_ON 	"(1)"
#define	MODULE_DBG_OFF 	"(0)"

int init_dbg();
int DebugPrintf(const char *format, ...);
int DebugPrint(const char *format, ...);
void set_dbg_limit(const char *buf);
long get_sys_runtime(int type);
char *get_cur_time(char *dst, int len);
void set_dbg_para(struct dbg_parame para);

/************************使用下面宏进行打印*****************************/
//可添加打印级别控制
#define DBG_PRINTF DebugPrintf  //带换行和warning error关键字的输出
#define DBG_PRINT DebugPrint  //不带换行的输出

//带文件和行号的普通打印(强制打印)
#define DBG_LINE(format,...) printf("[%s][%d]:" format "\r\n", __FILE__, __LINE__, ##__VA_ARGS__)

/**
 * 建议使用这个
 * 带调试级别(打印受APP级别和MODULE两个开关控制)
 * @param  APP_LEVEL    [APP打印级别]
 * @param  MODULE_LEVEL [模块打印级别]
 * @param  MODULE_NAME  [模块名称]
 * @param  CONTENT      [打印内容]
 */
#define DBG_LEVEL_STD(APP_LEVEL, MODULE_LEVEL, MODULE_NAME, CONTENT, ...)\
    DBG_PRINTF(APP_LEVEL MODULE_LEVEL "[%05ld.%03ld][%s][%s][%d]:" CONTENT, \
        get_sys_runtime(1), get_sys_runtime(2), MODULE_NAME, __FILE__, __LINE__, ##__VA_ARGS__)

/**
 * 带调试级别(打印只受APP级别控制)
 * @param  APP_LEVEL    [APP打印级别]
 * @param  MODULE_NAME  [模块名称]
 * @param  CONTENT      [打印内容]
 */
#define DBG_LEVEL_APP(APP_LEVEL, MODULE_NAME, CONTENT, ...)\
    DBG_PRINTF(APP_LEVEL MODULE_DBG_ON "[%05ld.%03ld][%s][%s][%d]:" CONTENT, \
        get_sys_runtime(1), get_sys_runtime(2), MODULE_NAME, __FILE__, __LINE__, ##__VA_ARGS__)

/**
 * 不带调试级别（一定会打印）
 * @param  MODULE_NAME  [模块名称]
 * @param  CONTENT      [打印内容]
 */
#define DBG_STD(MODULE_NAME, CONTENT, ...)\
    printf("[%05ld.%03ld][%s][%s][%d]:" CONTENT, \
        get_sys_runtime(1), get_sys_runtime(2), MODULE_NAME, __FILE__, __LINE__, ##__VA_ARGS__)

#endif
