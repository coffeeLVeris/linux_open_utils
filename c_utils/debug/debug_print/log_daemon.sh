#!/bin/sh
# $1:日志文件存放路径 $2：最大日志文件个数

logpath=$1
#echo $logpath

if [ ! -d $logpath ];then
	mkdir -p -m 755 $logpath
fi

while [ 1 ] ; do	

usage=`ls -l $logpath | wc -l`
while [ $usage -ge $2 ] ; do
	ls -l $logpath | head -2 | awk '{printf $9}' > /tmp/rm_arch
	tmpfilename=`cat /tmp/rm_arch`
	rmfilename="$logpath/$tmpfilename"
	rm -f $rmfilename
	echo "remove $rmfilename"
	usage=`ls -l $logpath | wc -l`
done

sleep 10
done	
