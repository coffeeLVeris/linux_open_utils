#include "debug_print.h"
#include <unistd.h>

#define MAIN_DBSW MODULE_DBG_ON
#define MAIN_DBNM "main module"

int main(int argc, char const *argv[])
{
    struct dbg_parame parame = {DEFAULT_DBG_LIMIT, DEFAULT_LOG_LIMIT, 500, "./test.log",
                                      false, 20, "./log_save", "./log_daemon.sh"};
    //可以不设置，使用默认值
    set_dbg_para(parame);
    if(init_dbg() < 0){
        return -1;
    }

    //设置默认打印级别，高于此级别的都会被打印出来
    set_dbg_limit(APP_INFO);

#if 1
    int i;
    int a = 5;
    unsigned char buf[10] = {0x12, 0x34, 0x56, 0x78, 0x90};

    //APP_DEBUG适用于不重要且一般不需要保留的调试信息
    DBG_LEVEL_STD(APP_DEBUG, MAIN_DBSW, MAIN_DBNM, "a = %d", a);
    //APP_INFO适用于需要保留的调试信息
    DBG_LEVEL_STD(APP_INFO, MAIN_DBSW, MAIN_DBNM, "a = %d", a);
    //APP_NOTICE适用于需要特别注意的调试信息
    DBG_LEVEL_STD(APP_NOTICE, MAIN_DBSW, MAIN_DBNM, "a = %d", a);
    //APP_WARNING适用于出现问题但是不会影响程序正常运行的输出
    DBG_LEVEL_STD(APP_WARNING, MAIN_DBSW, MAIN_DBNM , "a = %d", a);
    //APP_ERR适用于出现问题并且会导致出错的输出
    DBG_LEVEL_STD(APP_ERR, MAIN_DBSW, MAIN_DBNM, "a = %d", a);
    //APP_ALERT适用于会严重影响系统运行的状态输出
    DBG_LEVEL_STD(APP_ALERT, MAIN_DBSW, MAIN_DBNM, "a = %d", a);


    //字节流输出案例
    DBG_LEVEL_STD(APP_INFO, MAIN_DBSW, MAIN_DBNM, "buf len = %d", sizeof(buf));
    for(i=0; i<sizeof(buf); i++){
        DBG_PRINT(APP_INFO MAIN_DBSW "%02X ", buf[i]);
    }
    DBG_PRINT(APP_INFO MAIN_DBSW "\n");
#else //文件测试
    long i = 0;
    while (1) {
        DBG_LEVEL_STD(APP_WARNING, MAIN_DBSW, MAIN_DBNM, "i = %d", i++);
        usleep(100000);
    }
#endif
    return 0;
}
