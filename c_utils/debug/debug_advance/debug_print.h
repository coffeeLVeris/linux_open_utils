/**
 ** @author:   浓咖啡
 ** @date:     2019.7.18
 ** @brief:    log子系统进程
 */
 
#ifndef __DEBUG_PRINT_H
#define __DEBUG_PRINT_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>

#define true 1
#define false 0

#define KB 1024

#define DEFAULT_DBG_LIMIT  6    //APP_INFO(包括)以前会打印到终端
#define DEFAULT_LOG_LIMIT  4    //APP_WARNING(包括)之前的会同时输出到文件
#define DEFAULT_LOG_CUT_SIZE    1024*KB
#define DEFAULT_LOG_MAX_NUM    20

#define MAX_BUF_LEN 4096  //打印内容不能超过这个，否则会出段错误

////进程通信结构体
//struct dbg_msg{

//};

struct dbg_parame{
    int dbg_limit;  //打印级别控制，小于此级别会输出到终端
    int log_limit;  //日志级别控制，小于此级别会同时输出到日志
    int log_cut_size; //日志文件分割大小，到了这个大小会自动保存为新文件
    char log_file[256]; //日志文件路径和名称
    int start_daemon; //是否启动日志守护脚本 true:自动启动，需要指定下面参数 false:不自动启动，由系统启动
    int max_log_num; //最大存储多少日志文件
    char log_path[256]; //日志文件缓存路径
    char shell_file[256]; //守护脚本路径和名称
};

//总打印级别控制
#define	APP_EMERG	"<0>"	/* system is unusable			*/
#define	APP_ALERT	"<1>"	/* action must be taken immediately	*/
#define	APP_CRIT	"<2>"	/* critical conditions			*/
#define	APP_ERR         "<3>"	/* error conditions			*/
#define	APP_WARNING	"<4>"	/* warning conditions			*/
#define	APP_NOTICE	"<5>"	/* normal but significant condition	*/
#define	APP_INFO	"<6>"	/* informational			*/
#define	APP_DEBUG	"<7>"	/* debug-level messages			*/

int init_dbg();

int log(const char *format, ...);
int log_std(const char *format, ...);

void set_dbg_limit(const char *buf);
long get_sys_runtime(int type);
void set_dbg_para(struct dbg_parame para);



/************************使用下面宏进行打印*****************************/
#define LOG(APP_LEVEL, CONTENT, ...)\
    log(APP_LEVEL CONTENT, ##__VA_ARGS__)

#define LOG_STD(APP_LEVEL, MODULE_NAME, CONTENT, ...)\
    log_std(APP_LEVEL "[%05ld.%03ld]%s[%s][%s][%d]:" CONTENT, \
        get_sys_runtime(1), get_sys_runtime(2), APP_LEVEL, MODULE_NAME, __FILE__, __LINE__, ##__VA_ARGS__)

#endif
