/**
 ** @author:   浓咖啡
 ** @date:     2019.7.18
 ** @brief:    log子系统进程
 */
 
#include "debug_print.h"

static FILE *fp_log = NULL;

static struct dbg_parame parame = {DEFAULT_DBG_LIMIT, DEFAULT_LOG_LIMIT, DEFAULT_LOG_CUT_SIZE, "test.log",
                                  false, DEFAULT_LOG_MAX_NUM, "/log_save", "/etc/log_daemon.sh"};

static int init_log_file()
{
    int open_count = 0;

    while((fp_log==NULL) && (++open_count<4)){
        fp_log = fopen(parame.log_file, "a+");
    }

    if(fp_log == NULL){
        return -1;
    }else{
        return 0;
    }
}

void set_dbg_para(struct dbg_parame para)
{
    parame = para;
}

void set_dbg_limit(const char *buf)
{
    if((buf[0] == '<') && (buf[2] == '>')){
        int dbglevel = buf[1] - '0';
        if(dbglevel >= 0 && dbglevel <= 7){
            parame.dbg_limit = dbglevel;
        }
    }
}

int init_dbg()
{
    //首先创建日志文件
    if(init_log_file() < 0){
        printf("serious error:create %s\n", parame.log_file);
    }

    if(parame.start_daemon){
        //启动守护脚本
        if((access(parame.shell_file, F_OK)) < 0){
            printf("serious error:lack %s\n", parame.shell_file);
            return -1;
        }
        char cmd_tmp[256];
        sprintf(cmd_tmp, "%s %s %d &", \
                parame.shell_file, parame.log_path, parame.max_log_num);
        printf("start log_daemon.sh\n");
        system(cmd_tmp);
    }

    return 0;
}

/**
 * @brief 以当前时间生成文件名
 * @param dst
 */
static void get_time_name(char *dst)
{
    char tmp[256];
    time_t time_cur = time(NULL);
    strftime(tmp, sizeof(tmp), "%m%d-%H%M%S.log", localtime(&time_cur));
    strcpy(dst, tmp);
}


long get_sys_runtime(int type)
{
    struct timespec times = {0, 0};
    long time;

    clock_gettime(CLOCK_MONOTONIC, &times);

    if (1 == type){
        time = times.tv_sec;
    }else{
        time = times.tv_nsec / 1000000;
    }

    // printf("time = %ld\n", time);
    return time;
}

static char *get_cur_time(char *dst, int len)
{
    time_t timep;
    struct tm *ptr;

    timep = time(NULL);
    ptr = localtime(&timep);
    strftime(dst, len, "%m-%d %H:%M:%S", ptr);

    return dst;
}

int log_handle(const char *str_tmp)
{
    char cmd_tmp[256];
    const char *tmp;
    int dbglevel = DEFAULT_DBG_LIMIT;

    tmp = str_tmp;

    //printf("<4><1><5>");
    // 根据打印级别决定是否打印
    if((str_tmp[0] == '<') && (str_tmp[2] == '>')){
        dbglevel = str_tmp[1] - '0';
        if (dbglevel >= 0 && dbglevel <= 9){
            tmp = tmp + 3;
        }else{
            dbglevel = DEFAULT_DBG_LIMIT;
        }
    }

    if (dbglevel > parame.dbg_limit){
        return -1;
    }

    //如果当前级别小于日志级别，那么同时输出到日志文件
    if(dbglevel <= parame.log_limit){
        if(fp_log == NULL){
            printf("please init log file first\n");
            return -1;
        }
        fwrite(tmp, 1, strlen(tmp), fp_log);	//写入到日志文件,暂时不检测是否写入失败问题
        fflush(fp_log);
        if(ftell(fp_log) > parame.log_cut_size){  //如果日志文件大于限定的大小，那么暂存
            char new_name[256];
            get_time_name(new_name); //获取新的文件名
            //把当前日志拷贝到日志缓存目录中，并清空原文件
            sprintf(cmd_tmp, "cp %s %s/%s;cat /dev/null > %s;date > %s", \
                    parame.log_file, parame.log_path, new_name, parame.log_file, parame.log_file);
            system(cmd_tmp);
        }
    }

    printf("%s", tmp);

    return 0;

}

static int log_std_handle(const char *str_tmp)
{
    char cmd_tmp[256];
    const char *tmp;
    int dbglevel = DEFAULT_DBG_LIMIT;

    tmp = str_tmp;

    //printf("<4><1><5>");
    // 根据打印级别决定是否打印
    if((str_tmp[0] == '<') && (str_tmp[2] == '>')){
        dbglevel = str_tmp[1] - '0';
        if (dbglevel >= 0 && dbglevel <= 9){
            tmp = tmp + 3;
        }else{
            dbglevel = DEFAULT_DBG_LIMIT;
        }
    }

    if (dbglevel > parame.dbg_limit){
        return -1;
    }

    char str_final[MAX_BUF_LEN];
    switch (dbglevel) {
    case 5:
        sprintf(str_final, "\033[36m%s ------- notice\033[0m\r\n", tmp);
        break;
    case 4:
        sprintf(str_final, "\033[35m%s ------- warning\033[0m\r\n", tmp);
        break;
    case 3:
        sprintf(str_final, "\033[31m%s ------- error\033[0m\r\n", tmp);
        break;
    case 2:
        sprintf(str_final, "\033[31m%s ------- critical\033[31m\r\n", tmp);
        break;
    case 1:
        sprintf(str_final, "\033[33m%s ------- alert\033[0m\r\n", tmp);
        break;
    case 0:
        sprintf(str_final, "\033[31m%s ------- emergency\033[31m\r\n", tmp);
        break;
    default:
        sprintf(str_final, "%s\r\n", tmp);
        break;
    }


    printf("%s", str_final);

    //如果当前级别小于日志级别，那么同时输出到日志文件
    if(dbglevel <= parame.log_limit){
        if(fp_log == NULL){
            printf("please init log file first\n");
            return -1;
        }

        //增加当前时间
        char cur_time[50];
        get_cur_time(cur_time, 50);
        str_final[strlen(str_final)-2] = '\0'; //把增加的换行去掉
        sprintf(str_final, "%s ------- [%s]\r\n", str_final, cur_time);

        fwrite(str_final, 1, strlen(str_final), fp_log);	//写入到日志文件,暂时不检测是否写入失败问题
        fflush(fp_log);
        if(ftell(fp_log) > parame.log_cut_size){  //如果日志文件大于限定的大小，那么暂存
            char new_name[256];
            get_time_name(new_name); //获取新的文件名
            //把当前日志拷贝到日志缓存目录中，并清空原文件
            sprintf(cmd_tmp, "cp %s %s/%s;cat /dev/null > %s;date > %s", \
                    parame.log_file, parame.log_path, new_name, parame.log_file, parame.log_file);
            system(cmd_tmp);
        }
    }

    return 0;
}

/**
 * @brief 带所有打印信息的处理
 */
int log_std(const char *format, ...)
{
    char str_tmp[MAX_BUF_LEN];
    va_list arg;
    int len;

    va_start (arg, format);
    len = vsprintf(str_tmp, format, arg);
    va_end (arg);
    str_tmp[len] = '\0';

    //这里会把字符串通过unix发送出去，接收端调用下面方法处理
    log_std_handle(str_tmp);
    return 0;
}

/**
 * @brief 只有打印
 */
int log(const char *format, ...)
{
    char str_tmp[MAX_BUF_LEN];
    va_list arg;
    int len;

    va_start (arg, format);
    len = vsprintf(str_tmp, format, arg);
    va_end (arg);
    str_tmp[len] = '\0';

    //这里会把字符串通过unix发送出去，接收端调用下面方法处理
    log_handle(str_tmp);
    return 0;
}


