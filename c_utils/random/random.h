/**
 ** @author:   王世雄
 ** @date:     2019.3.11
 ** @brief:    封装linux随机数
 */
#ifndef RANDOM_H
#define RANDOM_H

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

int getRandomNum_range(int min, int max);
void getRandomNum_addr(char **dst, int num);

#endif

