/**
 ** @author:   王世雄
 ** @date:     2019.3.11
 ** @brief:    封装linux随机数
 */
#include "random.h"

int main()
{
	srand((unsigned int)time(NULL));

	printf( "%d\n", getRandomNum_range(-5, 2));
	
	char *p;
	int i;
	for( i = 1; i < 100; i++)
	{
		getRandomNum_addr(&p, i);
		printf( "%d位随机数:--------------- %s\n",i, p);
	}
}