/**
 ** @author:   王世雄
 ** @date:     2019.3.11
 ** @brief:    封装linux随机数
 */
#include "random.h"

//  左闭右闭区间，支持正负数
int getRandomNum_range(int min, int max)
{
	return rand()%(max - min + 1) + min;
}
//  支持任意位数字符串
void getRandomNum_addr(char **dst, int num)
{
	if(!num)
		return;
	int i;
	int max = 1;
        *dst = malloc(num+1);
	if(num <= 5)
	{
		for( i = 0; i < num;i++ )                          
			max *= 10;
		sprintf(*dst, "%d", rand()%(max - max/10) + max/10);
	}
	else
	{
		int a,b,i;
		a = num / 5;
		b = num % 5;
		for(i = 0; i < a; i++)
		{
			max = 100000;
			char *p = malloc(6);
			sprintf(p, "%d", rand()%(max - max/10) + max/10);
			strcat(*dst,p);
			free(p);
			p = NULL;
		}
		if(!b)
			return;
		char *p;
                getRandomNum_addr(&p, b);
		strcat(*dst,p);
		free(p);
		p = NULL;
	}
}

