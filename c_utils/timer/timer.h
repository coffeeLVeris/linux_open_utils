/**
 ** @author:   郭一甲
 ** @date:     2019.2.23
 ** @brief:    封装linux定时器
 */
 
#ifndef __TIMER_H__
#define __TIMER_H__

#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int timer(void (*fun)(void), struct timeval time);

#endif 
