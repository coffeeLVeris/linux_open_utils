/**
 ** @author:   郭一甲
 ** @date:     2019.2.23
 ** @brief:    封装linux定时器
 */
 
#include "timer.h"

static void tv1_func()
{
	printf("timer1 out\n");
}

static void tv2_func()
{
	printf("timer2 out\n");
}

int main(int argc, char const *argv[])
{
	struct timeval tv1, tv2;
	
	tv1.tv_sec = 2;
	tv1.tv_usec = 0;
	timer(tv1_func, tv1);
	
	tv2.tv_sec = 5;
	tv2.tv_usec = 300;

	timer(tv2_func, tv2);

	while(1);
	
	return 0;
}