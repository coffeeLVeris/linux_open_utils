#include <stdio.h>
#include <string.h>
#include "md5.h"

int main(int argc, char const *argv[])
{
    unsigned char org_pass[64] = "12345";
    unsigned char md5_pass[64];

    Md5Calc(md5_pass, org_pass, strlen(org_pass));

    printf("md5_pass = %s\n", md5_pass);

    return 0;
}