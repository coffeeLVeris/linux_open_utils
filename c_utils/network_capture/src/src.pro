TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    cap_manager.c \
    convert.c \
    pcap.c \
    raw.c

OTHER_FILES += \
    README.txt

HEADERS += \
    cap_manager.h \
    convert.h \
    stddef.h

