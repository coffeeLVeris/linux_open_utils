/**
 ** @author:	   浓咖啡
 ** @date:	   2017.4.5
 ** @brief:      管理两种抓包方式
 */

#include "cap_manager.h"

static cap_opt_pt cap_head = NULL;
static cap_opt_pt default_cap = NULL;  //默认的抓包方式

int register_cap_opt(cap_opt_pt new_cap)
{
    cap_opt_pt cap_tmp;

    if (!cap_head)
    {
        cap_head = new_cap;
        new_cap->next = NULL;
    }
    else
    {
        cap_tmp = cap_head;
        while (cap_tmp->next)
        {
            cap_tmp = cap_tmp->next;
        }
        cap_tmp->next = new_cap;
        new_cap->next = NULL;
    }

    return 0;
}

void show_cap_opt(void)
{
    int i = 0;
    cap_opt_pt tmp = cap_head;

    while (tmp)
    {
        printf("%02d %s\n", i++, tmp->name);
        tmp = tmp->next;
    }
}

cap_opt_pt get_cap_opt(const char *name)
{
    cap_opt_pt tmp = cap_head;

    while (tmp)
    {
        if (strcmp(tmp->name, name) == 0)
        {
            return tmp;
        }
        tmp = tmp->next;
    }

    return NULL;
}

/**
 * @brief 初始化默认抓包方式
 * @param name 抓包器名称
 * @param dev_str 设备名称
 * @param filter_str 过滤器字符串
 * @return 成功返回success, 失败返回fail
 */
bool init_cap(const char *name, char *dev_str, char *filter_str)
{
    default_cap = get_cap_opt(name);

    if(default_cap){
        return default_cap->cap_init(dev_str, filter_str);
    }else{
        return false;
    }
}

void start_cap()
{
    default_cap->start_cap();
}

void close_cap()
{
    default_cap->close_cap();
}

/**
 * @brief 把各个设备注册进来
 * @return
 */
int cap_init(void)
{
    int ret;

    ret = init_pcap();
    ret |= init_raw();

    return ret;
}
