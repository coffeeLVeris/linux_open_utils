#ifndef _CAP_MANAGER_H
#define _CAP_MANAGER_H

#include "convert.h"

typedef struct cap_opt {
    char *name;
    int (*cap_init)(char *dev_str, char *filter_str);
    void (*start_cap)(void);
    void (*close_cap)(void);
    struct cap_opt  *next;
}cap_opt_t, *cap_opt_pt;

int register_cap_opt(cap_opt_pt new_cap);
void show_cap_opt(void);
cap_opt_pt get_cap_opt(const char *name);
bool init_cap(const char *name, char *dev_str, char *filter_str);
void start_cap();
void close_cap();

int cap_init(void);

//具体设备函数
int init_pcap(void);
int init_raw(void);

#endif


