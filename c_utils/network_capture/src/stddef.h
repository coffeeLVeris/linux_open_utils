#ifndef _STDDEF_H
#define _STDDEF_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define true 0
#define false -1

#define success 0
#define fail -1

typedef int bool;

#if 1
#define DEBUG_ON
#else
#define DEBUG_OFF
#endif

#endif
