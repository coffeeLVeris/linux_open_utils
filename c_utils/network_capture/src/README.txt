使用说明：
1.执行make生成可执行文件pcap
2.以root用户运行，因为底层包需要这个权限
# ./capture -c "pcap" -d "eth0" -f "port 8888"
上面表示使用libpcap方式抓取经过eth0网卡并且端口是8888的数据包
./capture -c "raw"表示使用raw方式抓取经过eth0网卡数据包
原始套接字模式暂不支持过滤调条件设置，已在raw.c写死，修改请参照raw.c自行修改

3.windows下使用telnel 192.168.1.222 8888发数据包过去测试
4.最后编码后的包会存储到当前目录下的record.log 
5.使用hexdump命令查看数据包的16进制格式

root@ubuntu:/ceshi/app/pcap# hexdump record.log -C
00000000  7e 00 0c 29 cc 21 13 98  83 89 29 87 1e 08 00 45  |~..).!....)....E|
00000010  00 00 34 2c 26 40 00 40  06 89 71 c0 a8 01 fe c0  |..4,&@.@..q.....|
00000020  a8 01 de fe 89 22 b8 f2  72 ba ee 00 00 00 00 80  |....."..r.......|
00000030  02 20 00 fb 45 00 00 02  04 05 b4 01 03 03 02 01  |. ..E...........|
00000040  01 04 02 7e 7e 00 0c 29  cc 21 13 98 83 89 29 87  |...~~..).!....).|
00000050  1e 08 00 45 00 00 34 2c  29 40 00 40 06 89 6e c0  |...E..4,)@.@..n.|
00000060  a8 01 fe c0 a8 01 de fe  89 22 b8 f2 72 ba ee 00  |........."..r...|
00000070  00 00 00 80 02 20 00 fb  45 00 00 02 04 05 b4 01  |..... ..E.......|
00000080  03 03 02 01 01 04 02 7e  7e 00 0c 29 cc 21 13 98  |.......~~..).!..|
00000090  83 89 29 87 1e 08 00 45  00 00 30 2c 2b 40 00 40  |..)....E..0,+@.@|
000000a0  06 89 70 c0 a8 01 fe c0  a8 01 de fe 89 22 b8 f2  |..p.........."..|
000000b0  72 ba ee 00 00 00 00 70  02 20 00 0f 4f 00 00 02  |r......p. ..O...|
000000c0  04 05 b4 01 01 04 02 7e                           |.......~|
000000c8

每一帧以7e开头以7e结尾。