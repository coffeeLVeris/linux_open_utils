# LinuxOpenUtils #

## 项目特色 ##
集成linux平台下各种易用组件。方便在linux开发，尤其是嵌入式linux中加快开发速度。

## 运行说明 ##
linux平台，gcc/g++编译器。编辑器推荐使用qt creator或者Sublime Text

## 项目文档 ##
请移步wiki页面查看：
[https://gitee.com/coffeeLVeris/linux_open_utils/wikis](https://gitee.com/coffeeLVeris/linux_open_utils/wikis "项目文档")