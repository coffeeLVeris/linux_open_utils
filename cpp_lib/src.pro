TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    example/test.cpp \
    example/threadtest1.cpp \
    example/threadtest2.cpp \
    src/jglobalval.cpp \
    src/jkeyvalue.cpp \
    src/jmutex.cpp \
    src/jthread.cpp \
    src/jwaitcondition.cpp \
    src/datetime.cpp \
    src/gObject.cpp \
    example/sig_slot.cpp
HEADERS += \
    example/threadtest1.h \
    example/threadtest2.h \
    include/common.h \
    include/jglobalval.h \
    include/jkeyvalue.h \
    include/jmutex.h \
    include/jthread.h \
    include/jwaitcondition.h \
    include/datetime.h \
    src/gObject.h \
    example/sig_slot.h

INCLUDEPATH += include
