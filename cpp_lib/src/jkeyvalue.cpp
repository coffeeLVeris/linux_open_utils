#include <jkeyvalue.h>

JKeyValue::JKeyValue(const char *fileName)
{
    this->fileName = fileName;
    list.clear();
    bool ret = parseFile();
    if(!ret){
        cout<<"parseFile err"<<endl;
    }
}

JKeyValue::~JKeyValue()
{
    this->sync();
}

void JKeyValue::sync()
{
    FILE *file = fopen(fileName.data(), "a+");
    fflush(file);
    fclose(file);
}

bool JKeyValue::parseFile()
{
    char line[MAX_LEN];
    FILE *file = fopen(fileName.data(), "a+");
    if(file == NULL){
        perror("open init.cfg err:");
        return false;
    }

    struct item tmp;

    while(fgets(line, MAX_LEN, file) != NULL){
        line[strlen(line)-1] = '\0'; //去掉换行符
        char *p = strchr(line, '=');
        if(NULL == p){
            continue;
        }

        *p = '\0';  //把'='替换为结尾符
        p++;

        tmp.key = line;
        tmp.val = p;
        list.push_back(tmp);
    }

    //    for(int i=0; i<list.size(); i++){
    //        cout<<i<<" key="<<list.at(i).key<<" val="<<list.at(i).val<<endl;
    //    }

    fclose(file);
    return true;
}


string JKeyValue::value(const string &key)
{
    for(unsigned int i=0; i<list.size(); i++){
        if(list.at(i).key == key){
            return list.at(i).val;
        }
    }

    return string();
}

void JKeyValue::setValue(const string &key, const string &val)
{
    char record[MAX_LEN];

    //如果已经有此键值对，那么需要重写文件
    for(unsigned int i=0; i<list.size(); i++){
        if(list.at(i).key == key){
            list.at(i).val = val;
            //重写文件
            FILE *fileTmp = fopen("tmp", "w");  //临时文件

            for(unsigned int j=0; j<list.size(); j++){
                sprintf(record, "%s=%s\n",\
                        list.at(j).key.data(), list.at(j).val.data());
                fwrite(record, strlen(record), sizeof(char), fileTmp);
            }

            //删掉源文件，重命名临时文件尾为配置文件
            fclose(fileTmp);
            remove(fileName.data());//删除原文件
            rename("tmp", fileName.data());
            return;
        }
    }

    //执行到这表示没有这个键值对
    struct item tmp;
    tmp.key = key;
    tmp.val = val;
    list.push_back(tmp);

    FILE *file = fopen(fileName.data(), "a+");
    sprintf(record, "%s=%s\n",\
            key.data(), val.data());
    fwrite(record, strlen(record), sizeof(char), file);
    fclose(file);
}

bool JKeyValue::contains(const string &key)
{
    for(unsigned int i=0; i<list.size(); i++){
        if(list.at(i).key == key){
            return true;
        }
    }

    return false;
}

void JKeyValue::removeKey(const string &key)
{
    for(unsigned int i=0; i<list.size(); i++){
        if(list.at(i).key == key){
            list.erase(list.begin()+i);
            break;
        }
    }

    char record[MAX_LEN];
    FILE *fileTmp = fopen("tmp", "w");  //临时文件

    for(unsigned int j=0; j<list.size(); j++){
        sprintf(record, "%s=%s\n",\
                list.at(j).key.data(), list.at(j).val.data());
        fwrite(record, strlen(record), sizeof(char), fileTmp);
    }

    //删掉源文件，重命名临时文件尾为配置文件
    fclose(fileTmp);
    remove(fileName.data());//删除原文件
    rename("tmp", fileName.data());
}
