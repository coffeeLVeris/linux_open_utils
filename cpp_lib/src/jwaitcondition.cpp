#include <jwaitcondition.h>

JWaitCondition::JWaitCondition()
{
    pthread_cond_init(&condition, NULL);
}

void JWaitCondition::wait(JMutex *lockedMutex)
{
    pthread_mutex_t mutex = lockedMutex->getLock();
    pthread_cond_wait(&condition, &mutex);
}

void JWaitCondition::wakeOne()
{
    pthread_cond_signal(&condition);
}

void JWaitCondition::wakeAll()
{
    pthread_cond_broadcast(&condition);
}

JWaitCondition::~JWaitCondition()
{
    pthread_cond_destroy(&condition);
}
