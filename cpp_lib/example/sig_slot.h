#ifndef SIGSLOT_H
#define SIGSLOT_H

#include <gObject.h>
#include <string>

class SigSlot : public GObject
{
public:
    SigSlot(){}
    //定义一个名称为selected的信号；该信号接收两个参数，参数类型分别为const string&和int
    GSignal<const string&, int> selected;

public slots:
    void slot(const string &str, int idx);
};

#endif // SIGSLOT_H
