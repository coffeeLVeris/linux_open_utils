#include "threadtest1.h"

ThreadTest1::ThreadTest1()
{
}

void ThreadTest1::run()
{
    while (1) {
        cout << "th1 wait th2 user input" << endl;

        JGlobalVal::mutex.lock();
        JGlobalVal::cond.wait(&JGlobalVal::mutex);

        cout << "th2 input is " << JGlobalVal::globalStr <<endl<<endl;
        JGlobalVal::mutex.unlock();
    }
}
