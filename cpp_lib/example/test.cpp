#include <jkeyvalue.h>
#include <jwaitcondition.h>
#include "threadtest1.h"
#include "threadtest2.h"
#include "sig_slot.h"

#define SIG_SLOT

int test_fun(void)
{
#ifdef THREAD_TEST
    ThreadTest1 test1;
    ThreadTest2 test2;

    test1.start();
    test2.start();

    test1.wait();
    test2.wait();

#elif defined KEY_VALUE
    JKeyValue keyval;
    keyval.setValue("ip", "192.168.1.254");
    keyval.setValue("port", "8888");
#elif defined SIG_SLOT
    SigSlot *sigSlot = new SigSlot;
    GObject::connect(sigSlot, sigSlot->selected, sigSlot, &SigSlot::slot);
    sigSlot->selected.emit("hello slot", 27);
    GObject::disconnect(sigSlot, sigSlot->selected, sigSlot, &SigSlot::slot);

#endif
    return 0;
}












