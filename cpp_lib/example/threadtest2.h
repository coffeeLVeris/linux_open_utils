#ifndef THREADTEST2_H
#define THREADTEST2_H

#include "jglobalval.h"
#include "jthread.h"

class ThreadTest2 : public JThread
{
public:
    ThreadTest2();

protected:
    void run();
};

#endif // THREADTEST2_H
