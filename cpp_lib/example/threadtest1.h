#ifndef THREADTEST1_H
#define THREADTEST1_H

#include "jthread.h"
#include "jglobalval.h"

using namespace std;

class ThreadTest1 : public JThread
{
public:
    ThreadTest1();

protected:
    void run();
};

#endif // THREADTEST1_H
