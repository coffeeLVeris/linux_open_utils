#ifndef THREAD_H
#define THREAD_H

#include <iostream>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

using namespace std;

class JThread
{
public:
    enum THREAD_STATE{
        THREAD_STATUS_NEW = 0,
        THREAD_STATUS_RUNNING = 1,
        THREAD_STATUS_EXIT = -1
    };

    JThread();
    bool start();
    void stop();
    pthread_t getThreadID();
    int getState();
    void wait(unsigned long millisTime = 0);
    pthread_t getTid(){return tid;}

protected:
    //线程的运行实体
    virtual void run()=0;
    virtual void clearResource();
    bool flagRun;

private:
    //当前线程的线程ID
    pthread_t tid;
    //线程的状态
    int threadStatus;
    //使用静态方法也可以
    //static void * thread_proxy_func(void * args);
    friend void * thread_proxy_func(void * args);
    //内部执行方法
    void *__run();
};

class ThreadExample : public JThread
{
protected:
    void run()
    {
        while(flagRun){
            int number = 0;
            for (int i = 0; i < 10; i++)
            {
                cout << "Current number is " << number++;
                cout << " PID is " << getpid() << " TID is " << getThreadID() << endl;
                sleep(1);
            }
        }
    }
};

#endif
