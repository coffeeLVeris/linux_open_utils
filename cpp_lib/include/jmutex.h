#ifndef MUTEX_H
#define MUTEX_H

#include <pthread.h>
#include "common.h"

class JMutex
{
public:
    JMutex();
    ~JMutex();
    void lock();
    void unlock();
    bool isLocking() const {return isLock;}
    bool tryLock(int timeout = 0);
    pthread_mutex_t getLock(){return mutex;}

private:
    pthread_mutex_t mutex;
    bool isLock;
};

#endif // MUTEX_H
