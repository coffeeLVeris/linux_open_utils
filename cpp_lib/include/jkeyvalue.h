#ifndef KEYVALUE_H
#define KEYVALUE_H

#include "common.h"
#include <vector>

#define MAX_LEN 1024

struct item{
    string key;
    string val;
};

class JKeyValue
{
public:
    JKeyValue(const char *fileName = "init.cfg");
    ~JKeyValue();
    void sync();
    void setValue(const string &key, const string &val);
    string value(const string &key);
    bool contains(const string &key);
    void removeKey(const string &key);

private:
    string fileName;
    //FILE *file;
    vector<struct item> list;
    bool parseFile();
};

#endif // KEYVALUE_H
