#ifndef DATETIME_H
#define DATETIME_H

#include "common.h"
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <sys/timeb.h>
#include <sys/time.h>

class DateTime
{
public:
    DateTime();
    ~DateTime();
    void CurTime(const char * format);

private:
    struct tm *p;
    struct timeb tp_cur;
    time_t timep;

};


#endif // DATETIME_H
