#ifndef WAITCONDITION_H
#define WAITCONDITION_H

#include "jmutex.h"

class JWaitCondition
{
public:
    JWaitCondition();
    ~JWaitCondition();
    void wait(JMutex *);
    void wakeOne();
    void wakeAll();

private:
    pthread_cond_t condition;
};

#endif // WAITCONDITION_H
