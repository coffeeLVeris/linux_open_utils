#ifndef GLOBALVAL_H
#define GLOBALVAL_H

#include "jmutex.h"
#include "jwaitcondition.h"

class JGlobalVal
{
public:
    JGlobalVal();

    static JMutex mutex;
    static JWaitCondition cond;
    static string id;
    static string globalStr;
};

#endif // GLOBALVAL_H
